#!/usr/bin/perl

my $gsaminput = $ARGV[0];
my $outfile = defined $ARGV[1] ? $ARGV[1] : "volume.kml";

my %service = ();
my %billing = ();

#pull in data from file--------------------------------------------------------
open gsam, "<$gsaminput" or die "could not open file $gsaminput \n"
while (my $line = <gsam>)
{
	chomp $line;
	my @fields = split(/,/, $line);
	if (!($line =~ /.periodic,/))
	{
	next;
	}
	if ($#fileds == 5)
	{
		my  %geometries =
		(
			volname => shift(@fields),
			identif => shift(@fields),
			type => shift(@fields),
			lat => shift(@fileds),
			lon => shift(@fields),
			line =>	$line,
		);
		if (%geometries{type} == "Service")
		{
			$service = \%geometries;
		{
		if (%geometries{type} == "Billing")
		{
			$billing = \%geometries;
		}
	}
	else
	{
		print "ERR\n";
	}
}
close gsam;

#sort the data into arrays of the service and billing volumes------------------
while 
#==============================================================================
sub MakeHeader()
{
   my $outputfilename = shift;
   my @width = (2, 4);
   my $foldername = substr($outputfilename, 0, -4);
   return qq~<?xml version="1.0" encoding="UTF-8"?>
   <kml xmlns="http://earth.google.com/kml/2.2">
      <Document>
         <name>$foldername</name>
         <Style id="Service Volume">
              <LineStyle>
                  <color>ff00ff00</color>
                  <width>$width[0]</width>
              </LineStyle>
              <PolyStyle>
                  <color>8000ff00</color>
              </PolyStyle>
              <IconStyle>
                  <color>ff00ff00</color>
                  <Icon><href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png</href></Icon>
              </IconStyle>
         </Style>
         <Style id="Billing Volume">
              <LineStyle>
                  <color>ff0fdbff</color>
                  <width>$width[1]</width>
              </LineStyle>
              <PolyStyle>
                  <color>800fdbff</color>
              </PolyStyle>
              <IconStyle>
                  <color>ff0fdbff</color>
                  <Icon><href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png</href></Icon>
              </IconStyle>
         </Style>
   ~;
}
#==============================================================================
sub MakePolygon()
{
   my $name = shift;
   my $style = shift;
   my $visibility = shift;
   my $points = shift;

   my $s = qq~
      <Placemark>
         <name>$name</name>
         <styleUrl>#$style</styleUrl>
         <visibility>$visibility</visibility>
         <Polygon>
            <extrude>1</extrude>
            <tessellate>1</tessellate>
            <outerBoundaryIs>
               <LinearRing>
                  <coordinates>
   ~;
   foreach my $coord (@{ $points })
   {
      $s .= sprintf("%s,%s,%s ", $coord->{lon}, $coord->{lat}, $coord->{alt});
   }
   $s .= qq~
                  </coordinates>
               </LinearRing>
            </outerBoundaryIs>
         </Polygon>
      </Placemark>
   ~;

   return $s;
}
