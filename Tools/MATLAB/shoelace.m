function area = shoelace(x,y)

area=0.5*(sum(x.*circshift(y,length(y)-1))-sum(x.*circshift(y,1)));

end