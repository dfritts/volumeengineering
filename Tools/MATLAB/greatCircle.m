%Takes in a .csv of coordinates and calculates the great circle for each
%set of points with more than 500 nm between
%Notable functions: distance, gcwaypts
%Author: David Fritts

format long g 
rawcoor = csvread('coordinates.csv');
origcoor=rawcoor;
for index=(length(rawcoor)-1):-1:1    
    ptdist=deg2km(distance('rh',rawcoor(index,1),rawcoor(index,2),rawcoor(index+1,1),rawcoor(index+1,2)));
    if ptdist>=100
       ptsneeded=ceil(ptdist/50);
       gcpts= gcwaypts(rawcoor(index,1),rawcoor(index,2),rawcoor(index+1,1),rawcoor(index+1,2),ptsneeded);
       rawcoor = [rawcoor(1:index,:);gcpts;rawcoor(index+1:end,:)];
    end
end
dlmwrite('greatcoordinates.csv',rawcoor,'Delimiter',',','precision',9);
clear variables