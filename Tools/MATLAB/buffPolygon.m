function buffPolygon(filename, nmBuffer)
%Feed me the name of a .csv file containing the coordinates of the polygon
%in latlon
    format long g
    fid = fopen(filename);
    coor = textscan(fid, '%f%f', 'Delimiter', ',');
    buffer = nm2deg(nmBuffer);
    [outLat,outLon]=bufferm(cell(coor(:,1)),cell(coor(:,2)),buffer);
    dlmwrite('bufferedPolygon.csv',[outLat outLon], 'delimiter',',','precision',9);
    winopen('bufferedPolygon.csv');
end