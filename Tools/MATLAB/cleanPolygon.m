coor = csvread('coordinates.csv');

coorforward = circshift(coor,1);
badcoor = coor - coorforward;
badcoor = badcoor(:,1) + badcoor(:,2);

coor = coor(logical(badcoor),:);

% for index=1:(length(coor)-2)
%     if shoelace(coor(index:index+2,1),coor(index:index+2,2))==0
%         badcoor = [badcoor;index];
%     end
% end 

csvwrite('noduplicates.csv',coor);