﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLvalidations
{
    //Object Customer is the in memory representation of the Aireon customer.  It has a name, a bool for the cat023 enabled field, and a list of svols.
    //Mostly populated form the geojson files.
    class Customer
    {
        
        public string Name = "", DDID="";
        public bool CAT023enabled = false; 
        public List<SVOLinfo> SVOLS = new List<SVOLinfo>();

        public Customer(string Name)
        {
            this.Name = Name;

        }

        public Customer(Customer C, string Name)
        {
            this.Name = C.Name;

        }

    }
}
