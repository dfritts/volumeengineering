﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLvalidations
{
    //Oject SVOLinfo is the in memory stand in for a SVOL.  this is populated form the xml
    class SVOLinfo
    {


        public bool  has247 = false, has238SVOLDEF = false, has238SRVPRED = false, has253TLE = false,  has33 = false, has21 = false, has23STATS = false, has23ESR = false, has23STATUS=false, has25STATUS = false, has25STATS = false;
        public string SVOLID = "",ICAOID="", VolumeName;
        public int sac, sic, sid, port;
        public string unfilteredsac, unfilteredsic, unfilteredsid, multicast;
        


        public SVOLinfo()
        {


        }

        public SVOLinfo(SVOLinfo SV)
        {
            this.SVOLID = SV.SVOLID;
            this.has21 = SV.has21; this.has238SVOLDEF = SV.has238SVOLDEF; this.has238SRVPRED = SV.has238SRVPRED; this.has253TLE = SV.has253TLE;this.has33 = SV.has33; this.has21 = SV.has21; this.has23STATS = SV.has23STATS; this.has23ESR = SV.has23ESR; this.has23STATUS = SV.has23STATUS; this.has25STATUS = SV.has25STATUS; this.has25STATS = SV.has25STATS;
            this.port = SV.port;
        }

      
    }
}
