﻿// <auto-generated />
//
// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using XMLvalidations;
//
//    var welcome = Welcome.FromJson(jsonString);




//Ultimately not used, but good to keep around to experiment with.

namespace XMLvalidations
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Welcome
    {
        [JsonProperty("fileVersion")]
        public long FileVersion { get; set; }

        [JsonProperty("checksum")]
        public string Checksum { get; set; }

        [JsonProperty("fileFormatVersion")]
        public long FileFormatVersion { get; set; }

        [JsonProperty("fileName")]
        public string FileName { get; set; }

        [JsonProperty("fileGeneratedBy")]
        public string FileGeneratedBy { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("customerCollection")]
        public CustomerCollection[] CustomerCollection { get; set; }

        [JsonProperty("totalFeatures")]
        public long TotalFeatures { get; set; }

        [JsonProperty("features")]
        public Feature[] Features { get; set; }
    }

    public partial class CustomerCollection
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("properties")]
        public CustomerCollectionProperties Properties { get; set; }
    }

    public partial class CustomerCollectionProperties
    {
        [JsonProperty("customerID")]
        public CustomerId CustomerId { get; set; }

        [JsonProperty("customerName")]
        public string CustomerName { get; set; }

        [JsonProperty("customerRegion")]
        public string CustomerRegion { get; set; }

        [JsonProperty("customerRecordVersion")]
        public long CustomerRecordVersion { get; set; }

        [JsonProperty("customerEffectiveDate")]
        public DateTimeOffset CustomerEffectiveDate { get; set; }

        [JsonProperty("customerEndDate")]
        public DateTimeOffset CustomerEndDate { get; set; }

        [JsonProperty("anspTargetReportBundleTimeMsecs")]
        public long AnspTargetReportBundleTimeMsecs { get; set; }

        [JsonProperty("cat023StatusReportEnabled")]
        public bool Cat023StatusReportEnabled { get; set; }

        [JsonProperty("servicePredictionReportLookAheadHours")]
        public long ServicePredictionReportLookAheadHours { get; set; }

        [JsonProperty("rateServicePredictionReportMins")]
        public long RateServicePredictionReportMins { get; set; }

        [JsonProperty("rateServicePredictionDefinitionMins")]
        public long RateServicePredictionDefinitionMins { get; set; }

        [JsonProperty("rateVersionNumberReportMins")]
        public long RateVersionNumberReportMins { get; set; }

        [JsonProperty("DDID")]
        public string Ddid { get; set; }

        [JsonProperty("anspTargetReportVersion")]
        public string AnspTargetReportVersion { get; set; }

        [JsonProperty("servicePredictionReportTimeIncrementMins")]
        public long ServicePredictionReportTimeIncrementMins { get; set; }

        [JsonProperty("anspTargetReportTOA")]
        public bool AnspTargetReportToa { get; set; }

        [JsonProperty("anspTargetReportFineTOMR")]
        public bool AnspTargetReportFineTomr { get; set; }

        [JsonProperty("trackNumberFieldEnabled")]
        public bool TrackNumberFieldEnabled { get; set; }

        [JsonProperty("anspTargetReportPosType")]
        public bool AnspTargetReportPosType { get; set; }

        [JsonProperty("anspTargetReportTOMRv")]
        public bool AnspTargetReportTomRv { get; set; }

        [JsonProperty("anspTargetReportAGV")]
        public bool AnspTargetReportAgv { get; set; }

        [JsonProperty("anspTargetReportFTC0")]
        public bool AnspTargetReportFtc0 { get; set; }

        [JsonProperty("invalidMode3ACode")]
        public string InvalidMode3ACode { get; set; }

        [JsonProperty("duplicateType2SameSVol")]
        public bool DuplicateType2SameSVol { get; set; }

        [JsonProperty("anspTargetReportValidation")]
        public bool AnspTargetReportValidation { get; set; }

        [JsonProperty("anspTargetReportFailedState")]
        public bool AnspTargetReportFailedState { get; set; }

        [JsonProperty("sdpCollection")]
        public SdpCollection[] SdpCollection { get; set; }
    }

    public partial class SdpCollection
    {
        [JsonProperty("sdpId")]
        public string SdpId { get; set; }

        [JsonProperty("sdpName")]
        public string SdpName { get; set; }

        [JsonProperty("sdpLocation")]
        public string SdpLocation { get; set; }

        [JsonProperty("anspAssetIPAddress")]
        public string AnspAssetIpAddress { get; set; }

        [JsonProperty("anspAssetNetmask")]
        public string AnspAssetNetmask { get; set; }

        [JsonProperty("anspInterfaceDuplexMode")]
        public string AnspInterfaceDuplexMode { get; set; }

        [JsonProperty("anspInterfaceDuplexSpeed")]
        public long AnspInterfaceDuplexSpeed { get; set; }

        [JsonProperty("anspAssetDuplexType")]
        public string AnspAssetDuplexType { get; set; }

        [JsonProperty("serviceVolumeDelivery")]
        public ServiceVolumeDelivery[] ServiceVolumeDelivery { get; set; }
    }

    public partial class ServiceVolumeDelivery
    {
        [JsonProperty("serviceVolumeId")]
        public string ServiceVolumeId { get; set; }

        [JsonProperty("singleOrDualStream")]
        public SingleOrDualStream SingleOrDualStream { get; set; }

        [JsonProperty("unicastOrMulticastStream1")]
        public UnicastOrMulticastStream UnicastOrMulticastStream1 { get; set; }

        [JsonProperty("unicastOrMulticastStream2")]
        public UnicastOrMulticastStream UnicastOrMulticastStream2 { get; set; }
    }

    public partial class Feature
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("type")]
        public FeatureType Type { get; set; }

        [JsonProperty("bbox")]
        public double[] Bbox { get; set; }

        [JsonProperty("properties")]
        public FeatureProperties Properties { get; set; }

        [JsonProperty("geometry")]
        public Geometry Geometry { get; set; }
    }

    public partial class Geometry
    {
        [JsonProperty("type")]
        public GeometryType Type { get; set; }

        [JsonProperty("coordinates")]
        public double[][][] Coordinates { get; set; }
    }

    public partial class FeatureProperties
    {
        [JsonProperty("featureVersion")]
        public long FeatureVersion { get; set; }

        [JsonProperty("featureEffectiveDate")]
        public DateTimeOffset FeatureEffectiveDate { get; set; }

        [JsonProperty("featureBirthDate")]
        public DateTimeOffset FeatureBirthDate { get; set; }

        [JsonProperty("featureEndDate")]
        public DateTimeOffset FeatureEndDate { get; set; }

        [JsonProperty("customerID")]
        public CustomerId CustomerId { get; set; }

        [JsonProperty("volumeName")]
        public string VolumeName { get; set; }

        [JsonProperty("icaoID")]
        public string IcaoId { get; set; }

        [JsonProperty("sac")]
        public Sac Sac { get; set; }

        [JsonProperty("sic")]
        public string Sic { get; set; }

        [JsonProperty("sid")]
        public Sac Sid { get; set; }

        [JsonProperty("featureID")]
        public FeatureId FeatureId { get; set; }

        [JsonProperty("featureType")]
        public FeatureTypeEnum FeatureType { get; set; }

        [JsonProperty("serviceFeatureProperties")]
        public ServiceFeatureProperties ServiceFeatureProperties { get; set; }

        [JsonProperty("geometryType")]
        public GeometryTypeEnum GeometryType { get; set; }

        [JsonProperty("centerPoint")]
        public double[] CenterPoint { get; set; }

        [JsonProperty("floorFeet")]
        public long FloorFeet { get; set; }

        [JsonProperty("ceilingFeet")]
        public long CeilingFeet { get; set; }
    }

    public partial class ServiceFeatureProperties
    {
        [JsonProperty("serviceType")]
        public ServiceType ServiceType { get; set; }

        [JsonProperty("serviceDefaultMode")]
        public ServiceDefaultMode ServiceDefaultMode { get; set; }

        [JsonProperty("contractUpdateIntervalMsecs")]
        public long ContractUpdateIntervalMsecs { get; set; }

        [JsonProperty("contractLatencyMsecs")]
        public long ContractLatencyMsecs { get; set; }

        [JsonProperty("anspTargetReportUpdateMode")]
        public AnspTargetReportUpdateMode AnspTargetReportUpdateMode { get; set; }

        [JsonProperty("anspTargetReportMinWaitTimeMsecs")]
        public long AnspTargetReportMinWaitTimeMsecs { get; set; }

        [JsonProperty("anspTargetReportPeriodicRateMsecs")]
        public long AnspTargetReportPeriodicRateMsecs { get; set; }

        [JsonProperty("anspTestTargetReportRateSecs")]
        public long AnspTestTargetReportRateSecs { get; set; }

        [JsonProperty("anspTestTargetReportICAO")]
        public AnspTestTargetReportIcao AnspTestTargetReportIcao { get; set; }

        [JsonProperty("anspTestTargetReportPosition")]
        public double[] AnspTestTargetReportPosition { get; set; }

        [JsonProperty("anspTestTargetReportAltFeet")]
        public long AnspTestTargetReportAltFeet { get; set; }

        [JsonProperty("anspTestTargetReportMode3AEnabled")]
        public bool AnspTestTargetReportMode3AEnabled { get; set; }

        [JsonProperty("anspTestTargetReportTargetIDEnabled")]
        public bool AnspTestTargetReportTargetIdEnabled { get; set; }

        [JsonProperty("anspTestTargetReportTargetID")]
        public AnspTestTargetReportTargetId AnspTestTargetReportTargetId { get; set; }

        [JsonProperty("offlineStatusThresholdPercent")]
        public long OfflineStatusThresholdPercent { get; set; }

        [JsonProperty("degradedStatusThresholdPercent")]
        public long DegradedStatusThresholdPercent { get; set; }

        [JsonProperty("networkServicesAddress")]
        public string NetworkServicesAddress { get; set; }

        [JsonProperty("portTargetReportsCAT021")]
        public long PortTargetReportsCat021 { get; set; }

        [JsonProperty("portVersionNumberCAT247")]
        public long PortVersionNumberCat247 { get; set; }

        [JsonProperty("portAPDKeepAlive")]
        public long PortApdKeepAlive { get; set; }

        [JsonProperty("portEquipmentStatusCAT023")]
        public long PortEquipmentStatusCat023 { get; set; }

        [JsonProperty("portServiceVolumeStatusCAT023")]
        public long PortServiceVolumeStatusCat023 { get; set; }

        [JsonProperty("portServiceVolumeStatisticsCAT023")]
        public long PortServiceVolumeStatisticsCat023 { get; set; }

        [JsonProperty("portServiceDefinitionReportCAT238")]
        public long PortServiceDefinitionReportCat238 { get; set; }

        [JsonProperty("portServicePredictionReportCAT238")]
        public long PortServicePredictionReportCat238 { get; set; }

        [JsonProperty("portServiceSystemStatusReportCAT025")]
        public long PortServiceSystemStatusReportCat025 { get; set; }

        [JsonProperty("portServiceStatisticsReportCAT025")]
        public long PortServiceStatisticsReportCat025 { get; set; }

        [JsonProperty("portConstellationTLEReportCAT253")]
        public long PortConstellationTleReportCat253 { get; set; }

        [JsonProperty("portCat033")]
        public long PortCat033 { get; set; }

        [JsonProperty("portCat033BelowMinLV")]
        public long PortCat033BelowMinLv { get; set; }

        [JsonProperty("portCat033DupType1")]
        public long PortCat033DupType1 { get; set; }

        [JsonProperty("portCat033DupType1BelowMinLV")]
        public long PortCat033DupType1BelowMinLv { get; set; }

        [JsonProperty("portCat033InvMode3A")]
        public long PortCat033InvMode3A { get; set; }

        [JsonProperty("portCat033InvMode3ABelowMinLV")]
        public long PortCat033InvMode3ABelowMinLv { get; set; }

        [JsonProperty("latencyTPM")]
        public bool LatencyTpm { get; set; }

        [JsonProperty("updateIntervalTPM")]
        public bool UpdateIntervalTpm { get; set; }

        [JsonProperty("probabilityUpdateTPM")]
        public bool ProbabilityUpdateTpm { get; set; }

        [JsonProperty("probabilityLongGapTPM")]
        public bool ProbabilityLongGapTpm { get; set; }

        [JsonProperty("serviceDesignator")]
        public ServiceDesignator ServiceDesignator { get; set; }

        [JsonProperty("serviceMode")]
        public ServiceMode ServiceMode { get; set; }

        [JsonProperty("expectedTargetLoad")]
        public long ExpectedTargetLoad { get; set; }

        [JsonProperty("sendSurfaceTargets")]
        public bool SendSurfaceTargets { get; set; }

        [JsonProperty("minLinkVersion")]
        public long MinLinkVersion { get; set; }

        [JsonProperty("anspTargetReportRxID1")]
        public long AnspTargetReportRxId1 { get; set; }

        [JsonProperty("gracefulDegradation")]
        public bool GracefulDegradation { get; set; }

        [JsonProperty("gracefulDegradationLoad")]
        public long GracefulDegradationLoad { get; set; }

        [JsonProperty("gracefulDegradationRate")]
        public double GracefulDegradationRate { get; set; }

        [JsonProperty("serviceVolumeId")]
        public string ServiceVolumeId { get; set; }

        [JsonProperty("rateTLESecs")]
        public long RateTleSecs { get; set; }

        [JsonProperty("rateEquipmentStatusSecs")]
        public long RateEquipmentStatusSecs { get; set; }

        [JsonProperty("rateServiceVolumeStatusSecs")]
        public long RateServiceVolumeStatusSecs { get; set; }

        [JsonProperty("rateServiceVolumeStatisticsSecs")]
        public long RateServiceVolumeStatisticsSecs { get; set; }

        [JsonProperty("anspTestTargetReportTargetsNIC")]
        public long AnspTestTargetReportTargetsNic { get; set; }

        [JsonProperty("anspTestTargetReportNoTargetsNIC")]
        public long AnspTestTargetReportNoTargetsNic { get; set; }

        [JsonProperty("anspTestTargetReportTargetsNACp")]
        public long AnspTestTargetReportTargetsNaCp { get; set; }

        [JsonProperty("anspTestTargetReportNoTargetsNACp")]
        public long AnspTestTargetReportNoTargetsNaCp { get; set; }
    }

    public enum CustomerId { Cus1000015 };

    public enum SingleOrDualStream { Single };

    public enum UnicastOrMulticastStream { Multicast };

    public enum GeometryType { Polygon };

    public enum FeatureId { The0X00 };

    public enum FeatureTypeEnum { Service };

    public enum GeometryTypeEnum { Polygon };

    public enum Sac { The0X01, The0X07 };

    public enum AnspTargetReportUpdateMode { Update };

    public enum AnspTestTargetReportIcao { The0Xc1F900 };

    public enum AnspTestTargetReportTargetId { Aireon01 };

    public enum ServiceDefaultMode { Active };

    public enum ServiceDesignator { The1090Adsb };

    public enum ServiceMode { Operational };

    public enum ServiceType { Primary };

    public enum FeatureType { Feature };

    public partial class Welcome
    {
        public static Welcome FromJson(string json) => JsonConvert.DeserializeObject<Welcome>(json, XMLvalidations.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this Welcome self) => JsonConvert.SerializeObject(self, XMLvalidations.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                CustomerIdConverter.Singleton,
                SingleOrDualStreamConverter.Singleton,
                UnicastOrMulticastStreamConverter.Singleton,
                GeometryTypeConverter.Singleton,
                FeatureIdConverter.Singleton,
                FeatureTypeEnumConverter.Singleton,
                GeometryTypeEnumConverter.Singleton,
                SacConverter.Singleton,
                AnspTargetReportUpdateModeConverter.Singleton,
                AnspTestTargetReportIcaoConverter.Singleton,
                AnspTestTargetReportTargetIdConverter.Singleton,
                ServiceDefaultModeConverter.Singleton,
                ServiceDesignatorConverter.Singleton,
                ServiceModeConverter.Singleton,
                ServiceTypeConverter.Singleton,
                FeatureTypeConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class CustomerIdConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(CustomerId) || t == typeof(CustomerId?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "CUS-1000015")
            {
                return CustomerId.Cus1000015;
            }
            throw new Exception("Cannot unmarshal type CustomerId");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (CustomerId)untypedValue;
            if (value == CustomerId.Cus1000015)
            {
                serializer.Serialize(writer, "CUS-1000015");
                return;
            }
            throw new Exception("Cannot marshal type CustomerId");
        }

        public static readonly CustomerIdConverter Singleton = new CustomerIdConverter();
    }

    internal class SingleOrDualStreamConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(SingleOrDualStream) || t == typeof(SingleOrDualStream?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "single")
            {
                return SingleOrDualStream.Single;
            }
            throw new Exception("Cannot unmarshal type SingleOrDualStream");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (SingleOrDualStream)untypedValue;
            if (value == SingleOrDualStream.Single)
            {
                serializer.Serialize(writer, "single");
                return;
            }
            throw new Exception("Cannot marshal type SingleOrDualStream");
        }

        public static readonly SingleOrDualStreamConverter Singleton = new SingleOrDualStreamConverter();
    }

    internal class UnicastOrMulticastStreamConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(UnicastOrMulticastStream) || t == typeof(UnicastOrMulticastStream?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "multicast")
            {
                return UnicastOrMulticastStream.Multicast;
            }
            throw new Exception("Cannot unmarshal type UnicastOrMulticastStream");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (UnicastOrMulticastStream)untypedValue;
            if (value == UnicastOrMulticastStream.Multicast)
            {
                serializer.Serialize(writer, "multicast");
                return;
            }
            throw new Exception("Cannot marshal type UnicastOrMulticastStream");
        }

        public static readonly UnicastOrMulticastStreamConverter Singleton = new UnicastOrMulticastStreamConverter();
    }

    internal class GeometryTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(GeometryType) || t == typeof(GeometryType?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "Polygon")
            {
                return GeometryType.Polygon;
            }
            throw new Exception("Cannot unmarshal type GeometryType");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (GeometryType)untypedValue;
            if (value == GeometryType.Polygon)
            {
                serializer.Serialize(writer, "Polygon");
                return;
            }
            throw new Exception("Cannot marshal type GeometryType");
        }

        public static readonly GeometryTypeConverter Singleton = new GeometryTypeConverter();
    }

    internal class FeatureIdConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(FeatureId) || t == typeof(FeatureId?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "0x00")
            {
                return FeatureId.The0X00;
            }
            throw new Exception("Cannot unmarshal type FeatureId");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (FeatureId)untypedValue;
            if (value == FeatureId.The0X00)
            {
                serializer.Serialize(writer, "0x00");
                return;
            }
            throw new Exception("Cannot marshal type FeatureId");
        }

        public static readonly FeatureIdConverter Singleton = new FeatureIdConverter();
    }

    internal class FeatureTypeEnumConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(FeatureTypeEnum) || t == typeof(FeatureTypeEnum?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "service")
            {
                return FeatureTypeEnum.Service;
            }
            throw new Exception("Cannot unmarshal type FeatureTypeEnum");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (FeatureTypeEnum)untypedValue;
            if (value == FeatureTypeEnum.Service)
            {
                serializer.Serialize(writer, "service");
                return;
            }
            throw new Exception("Cannot marshal type FeatureTypeEnum");
        }

        public static readonly FeatureTypeEnumConverter Singleton = new FeatureTypeEnumConverter();
    }

    internal class GeometryTypeEnumConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(GeometryTypeEnum) || t == typeof(GeometryTypeEnum?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "polygon")
            {
                return GeometryTypeEnum.Polygon;
            }
            throw new Exception("Cannot unmarshal type GeometryTypeEnum");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (GeometryTypeEnum)untypedValue;
            if (value == GeometryTypeEnum.Polygon)
            {
                serializer.Serialize(writer, "polygon");
                return;
            }
            throw new Exception("Cannot marshal type GeometryTypeEnum");
        }

        public static readonly GeometryTypeEnumConverter Singleton = new GeometryTypeEnumConverter();
    }

    internal class SacConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Sac) || t == typeof(Sac?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "0x01":
                    return Sac.The0X01;
                case "0x07":
                    return Sac.The0X07;
            }
            throw new Exception("Cannot unmarshal type Sac");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Sac)untypedValue;
            switch (value)
            {
                case Sac.The0X01:
                    serializer.Serialize(writer, "0x01");
                    return;
                case Sac.The0X07:
                    serializer.Serialize(writer, "0x07");
                    return;
            }
            throw new Exception("Cannot marshal type Sac");
        }

        public static readonly SacConverter Singleton = new SacConverter();
    }

    internal class AnspTargetReportUpdateModeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(AnspTargetReportUpdateMode) || t == typeof(AnspTargetReportUpdateMode?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "update")
            {
                return AnspTargetReportUpdateMode.Update;
            }
            throw new Exception("Cannot unmarshal type AnspTargetReportUpdateMode");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (AnspTargetReportUpdateMode)untypedValue;
            if (value == AnspTargetReportUpdateMode.Update)
            {
                serializer.Serialize(writer, "update");
                return;
            }
            throw new Exception("Cannot marshal type AnspTargetReportUpdateMode");
        }

        public static readonly AnspTargetReportUpdateModeConverter Singleton = new AnspTargetReportUpdateModeConverter();
    }

    internal class AnspTestTargetReportIcaoConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(AnspTestTargetReportIcao) || t == typeof(AnspTestTargetReportIcao?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "0xc1f900")
            {
                return AnspTestTargetReportIcao.The0Xc1F900;
            }
            throw new Exception("Cannot unmarshal type AnspTestTargetReportIcao");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (AnspTestTargetReportIcao)untypedValue;
            if (value == AnspTestTargetReportIcao.The0Xc1F900)
            {
                serializer.Serialize(writer, "0xc1f900");
                return;
            }
            throw new Exception("Cannot marshal type AnspTestTargetReportIcao");
        }

        public static readonly AnspTestTargetReportIcaoConverter Singleton = new AnspTestTargetReportIcaoConverter();
    }

    internal class AnspTestTargetReportTargetIdConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(AnspTestTargetReportTargetId) || t == typeof(AnspTestTargetReportTargetId?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "AIREON01")
            {
                return AnspTestTargetReportTargetId.Aireon01;
            }
            throw new Exception("Cannot unmarshal type AnspTestTargetReportTargetId");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (AnspTestTargetReportTargetId)untypedValue;
            if (value == AnspTestTargetReportTargetId.Aireon01)
            {
                serializer.Serialize(writer, "AIREON01");
                return;
            }
            throw new Exception("Cannot marshal type AnspTestTargetReportTargetId");
        }

        public static readonly AnspTestTargetReportTargetIdConverter Singleton = new AnspTestTargetReportTargetIdConverter();
    }

    internal class ServiceDefaultModeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(ServiceDefaultMode) || t == typeof(ServiceDefaultMode?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "active")
            {
                return ServiceDefaultMode.Active;
            }
            throw new Exception("Cannot unmarshal type ServiceDefaultMode");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (ServiceDefaultMode)untypedValue;
            if (value == ServiceDefaultMode.Active)
            {
                serializer.Serialize(writer, "active");
                return;
            }
            throw new Exception("Cannot marshal type ServiceDefaultMode");
        }

        public static readonly ServiceDefaultModeConverter Singleton = new ServiceDefaultModeConverter();
    }

    internal class ServiceDesignatorConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(ServiceDesignator) || t == typeof(ServiceDesignator?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "1090ADSB")
            {
                return ServiceDesignator.The1090Adsb;
            }
            throw new Exception("Cannot unmarshal type ServiceDesignator");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (ServiceDesignator)untypedValue;
            if (value == ServiceDesignator.The1090Adsb)
            {
                serializer.Serialize(writer, "1090ADSB");
                return;
            }
            throw new Exception("Cannot marshal type ServiceDesignator");
        }

        public static readonly ServiceDesignatorConverter Singleton = new ServiceDesignatorConverter();
    }

    internal class ServiceModeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(ServiceMode) || t == typeof(ServiceMode?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "operational")
            {
                return ServiceMode.Operational;
            }
            throw new Exception("Cannot unmarshal type ServiceMode");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (ServiceMode)untypedValue;
            if (value == ServiceMode.Operational)
            {
                serializer.Serialize(writer, "operational");
                return;
            }
            throw new Exception("Cannot marshal type ServiceMode");
        }

        public static readonly ServiceModeConverter Singleton = new ServiceModeConverter();
    }

    internal class ServiceTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(ServiceType) || t == typeof(ServiceType?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "primary")
            {
                return ServiceType.Primary;
            }
            throw new Exception("Cannot unmarshal type ServiceType");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (ServiceType)untypedValue;
            if (value == ServiceType.Primary)
            {
                serializer.Serialize(writer, "primary");
                return;
            }
            throw new Exception("Cannot marshal type ServiceType");
        }

        public static readonly ServiceTypeConverter Singleton = new ServiceTypeConverter();
    }

    internal class FeatureTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(FeatureType) || t == typeof(FeatureType?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "Feature")
            {
                return FeatureType.Feature;
            }
            throw new Exception("Cannot unmarshal type FeatureType");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (FeatureType)untypedValue;
            if (value == FeatureType.Feature)
            {
                serializer.Serialize(writer, "Feature");
                return;
            }
            throw new Exception("Cannot marshal type FeatureType");
        }

        public static readonly FeatureTypeConverter Singleton = new FeatureTypeConverter();
    }
}
