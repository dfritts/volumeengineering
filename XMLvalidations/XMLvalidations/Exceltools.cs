﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using System.Windows.Forms;

namespace XMLvalidations
{
    class Exceltools
    {
        //returns the row number of the next given a sheet and a row number
       public static int nextblankcelllocation(Microsoft.Office.Interop.Excel.Worksheet sheet, int rownumber, int colnumber)
        {
            bool foundit = false;
            int thingtoreturn = 1;
            for (int i = rownumber; i < 400000; i++)
            {
                if (sheet.Cells[i, colnumber].Value == null)
                {
                    foundit = true;
                    thingtoreturn = i;
                    break;

                }

            }


            return thingtoreturn;
        }

        //finds first nonblank cell
        public static int nextnonblankcelllocation(Microsoft.Office.Interop.Excel.Worksheet sheet, int rownumber, int colnumber, int lastnum)
        {
            if (lastnum <= 0)
                lastnum = 40000;
            bool foundit = false;
            int thingtoreturn = 1;
            for (int i = rownumber; i < lastnum; i++)
            {
                if (sheet.Cells[i, colnumber].Value != null)
                {
                    foundit = true;
                    thingtoreturn = i;
                    break;

                }

            }
            if (!foundit)
                thingtoreturn = lastnum;

            return thingtoreturn;
        }

        //For the new Tower format
        public static int Towernextblankcelllocation(Microsoft.Office.Interop.Excel.Worksheet sheet, int rownumber, int colnumber)
        {
            bool foundit = false;
            int thingtoreturn = 1;
            for (int i = rownumber; i < 400000; i=i+12)
            {
                if (sheet.Cells[i, colnumber].Value == null)
                {
                    foundit = true;
                    thingtoreturn = i+12;
                    break;

                }

            }


            return thingtoreturn;
        }

        public static int Towernextrowblankcelllocation(Microsoft.Office.Interop.Excel.Worksheet sheet, int rownumber, int startcol)
        {
            bool foundit = false;
            int thingtoreturn = 1;
            for (int i = startcol; i < 400000; i=i+12)
            {
                if (sheet.Cells[rownumber, i].Value == null)
                {
                    foundit = true;
                    thingtoreturn = i+12;
                    break;

                }

            }


            return thingtoreturn;
        }


        //goes down a given column until the a cell with the given value is reached

        public static int findincolumn(string value, int rownum, int colnum, Microsoft.Office.Interop.Excel.Worksheet sheet)
        {
            int thingtoreturn = 1;
            int nullcount = 0;
            for (int i = rownum; i < 400000; i++)
            {
                if (sheet.Cells[i, colnum].Value != null)
                {
                    if (sheet.Cells[i, colnum].Value2.ToString().Contains(value))
                    {
                        thingtoreturn = i;
                        break;
                    }

                }

                else
                    nullcount++;

                if (nullcount > 1000)
                {
                    return 0;
                }

            }

            return thingtoreturn;

        }



        //goes down a given column until the a cell with the given value is reached

        public static int findinrow(string value, int rownum, int colnum, Microsoft.Office.Interop.Excel.Worksheet sheet)
        {
            int thingtoreturn = 1;
            int nullcount = 0;
            for (int i = rownum; i < 400000; i++)
            {
                if (sheet.Cells[rownum, i].Value != null)
                {
                    if (sheet.Cells[rownum, i].Value2.ToString().Contains(value))
                    {
                        thingtoreturn = i;
                        break;
                    }

                }

                else
                    nullcount++;

                if (nullcount > 1000)
                {
                    return 0;
                }

            }

            return thingtoreturn;

        }

        //find first blank horizontal cell
        public static int nextrowblankcelllocation(Microsoft.Office.Interop.Excel.Worksheet sheet, int rownumber, int startcol)
        {
            bool foundit = false;
            int thingtoreturn = 1;
            for (int i = startcol; i < 400000; i++)
            {
                if (sheet.Cells[rownumber, i].Value == null)
                {
                    foundit = true;
                    thingtoreturn = i;
                    break;

                }

            }


            return thingtoreturn;
        }

        //returns a list of ints of positions for an incoming list of strings of needed names and a string that is the header (with a delimiter).  NOTE: the list of meeded titles currently has to be in the order they appear in the string line.
        public static List<int> Headertop(string titleline, char delimiter, List<string> Line, string location)
        {
            bool ispipe = false;
            List<int> headernumberlist = new List<int>();

            if (delimiter == '|')
                ispipe = true;
            List<int> titlepositions = new List<int>();
            //This method returns an array matching the given title string with its corresponding position across a songle line of pipe delimited text
            if (titleline != "")
            {
                var piece = titleline.Split('|');
                if (!ispipe)
                    piece = titleline.Split(',');

                for (int i = 0; i < titleline.Count() - 1; i++)
                {
                    try
                    {
                        for (int z = 0; z < Line.Count; z++)
                        {
                            if (Line[z].Contains(piece[i]))
                            {
                                if (piece[i] != "")
                                {
                                    titlepositions.Add(i);
                                    break;
                                }
                            }

                        }
                    }
                    catch
                    { }
                }

                if (titlepositions.Count != Line.Count)
                    MessageBox.Show("Mismatch in titles in " + location);

                return titlepositions;
            }
            else
                return titlepositions;
        }

        //returns active sheet from a string file location of an excel workbook
        public static Microsoft.Office.Interop.Excel.Worksheet Returnactivesheetsheet (string fileloc)
        {
            Microsoft.Office.Interop.Excel.Application returnsol = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook returnbook;
            Microsoft.Office.Interop.Excel.Worksheet returnsheet;

            returnsol.Workbooks.Open(fileloc);
            returnbook = returnsol.ActiveWorkbook;
            returnsheet = returnbook.ActiveSheet;

            return returnsheet;

        }

        //returns the sheet from the name, or if the sheet does not exists, creates a sheet with that name.
        public static Microsoft.Office.Interop.Excel.Workbook Returnnamedsheetbook(string fileloc, string sheetname, Microsoft.Office.Interop.Excel.Application returnsol, Microsoft.Office.Interop.Excel.Workbook returnbook)
        {

            Microsoft.Office.Interop.Excel.Worksheet returnsheet;

            returnsol.Workbooks.Open(fileloc);
            returnbook = returnsol.ActiveWorkbook;

            try
            {
                returnsheet = returnbook.Worksheets[sheetname];
            }

            catch
            {
                returnsheet = returnbook.Worksheets.Add();
                returnsheet = returnbook.ActiveSheet;
                returnsheet.Name = sheetname;

            }
            returnsheet.Activate();
            return returnbook;
        }

        public static void Saveexcelbook(Microsoft.Office.Interop.Excel.Worksheet savesheet, string savelocation)
        {
            Microsoft.Office.Interop.Excel.Application saveapp = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook savebook;
            savebook = saveapp.ActiveWorkbook;
            
            saveapp.DisplayAlerts = false;
            savebook.SaveAs(savelocation);
            savebook.Close();
            saveapp.Quit();

        }

    }
}
