﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using XML = System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Text.RegularExpressions;


namespace XMLvalidations
{
    class Program
    {

        /*
         * This program is intended as a validation check for appConfig.xml files to ensure the appropriate categories of messages are activated for given customer geojson files.
         * Input: all customer geojson files the user desires to check against, the XML file which contains the data for the related customers
         * Output: Excel file laid out as follows with all CAtegory fields T/F:
         *          Customer, SVol name, Svol Mulsticast, raw sac, raw sic, daw sid, CAT023 enabled, CAT247, CAT238Svoldef, CAT238 Service Prediction, CAT253 TLE,	CAT033,	CAT021,	CAT023 Stats, CAT023 ESR, CAT023 status, CAT025 status, CAT025 stats, Volume Name
         *
         * How to use:
         *      1) Put all inputs into the same folder containing nothing but desired input files
         *      2) Run this program
         *      3) When prompted copy and paste the folder location into the dialogue box
         *      4) Press Enter
         *      
         */

        //declaration of public variables: folder location to be input by user, list of service volumes (currently not used in final report), list of customers
        static string folderloc = "";
        static List<SVOLinfo> listofSVOLS = new List<SVOLinfo>();
        static List<Customer> listofcustomers = new List<Customer>();

        //Starting Point
        static void Main(string[] args)
        {
            //get input location from user
            Console.WriteLine("Please enter folder location");
            folderloc = Console.ReadLine();


            findfiles();
        }
        
        //Churns through folder for the requisite files
        static void findfiles()
        {
            string xmlname = "";
            string excelname = "";
            string gjname = "";
            List<string> gjnames = new List<string>();

            //Method catches inputs that are not actual folders
            try
            {
                //Sets the working directory as the current directory
                Directory.SetCurrentDirectory(folderloc);

            }
            catch
            {
                //trying again
                Console.WriteLine("Please enter a valid folder location");
                folderloc = Console.ReadLine();

                //Sets the working directory as the current directory
                Directory.SetCurrentDirectory(folderloc);
            }

            DirectoryInfo di = new DirectoryInfo(Environment.CurrentDirectory);
            //Looks through folder to and classify the appropriate files.  Avoids files that are open by another program  
            FileInfo[] fi = di.GetFiles();
            for (int i = 0; i < fi.Length; i++)
            {
                if (fi[i].Name.Contains(".xml") && !fi[i].Name.Contains("~"))
                    xmlname = fi[i].FullName;
                if(fi[i].Name.Contains("geojson") && !fi[i].Name.Contains("~"))
                {
                    gjname = fi[i].Name;
                    gjnames.Add(gjname);

                }
            }
            

            processesgjfiles(gjnames);

            ProcessXMLfile(xmlname);

            printexcelfile(excelname,xmlname);
        }

        //processes geojson files and builds internal customer list
        static void processesgjfiles(List<string> listofgjnames)
        {
           //Iterates through all geojson files in the given folder
            for(int gjfileiter=0; gjfileiter < listofgjnames.Count; gjfileiter++)
            {
                Customer thiscustomer = new Customer("");
                List<SVOLinfo> thislist = new List<SVOLinfo>();

                //Reads in entire GeoJSON for parsing
                string location = folderloc + @"\" + listofgjnames[gjfileiter];
                StreamReader SR = new StreamReader(location);
                string sting = SR.ReadToEnd();
                dynamic gjchurn = JsonConvert.DeserializeObject<dynamic>(sting);

                dynamic coll = gjchurn.customerCollection;

                //iterates through the relevant pars of the geojson file to find attributes needed
                foreach (var toplevelthing in coll.Children())
                {
                    foreach (var secondlevelthing in toplevelthing)
                    {
                        if (secondlevelthing.Name == "properties")
                        {
                            foreach (var thirdlevelthing in secondlevelthing)
                            {
                                foreach(var fourthlevelthing in thirdlevelthing)
                                {
                                    if (fourthlevelthing.Name == "customerName")
                                        thiscustomer.Name = fourthlevelthing.Value;
                                    else if (fourthlevelthing.Name == "cat023StatusReportEnabled")
                                    {
                                        string value = fourthlevelthing.Value;
                                        if (value.Contains("true") || value.Contains("True"))
                                            thiscustomer.CAT023enabled = true;
                                    }

                                    else if (fourthlevelthing == "DDID")
                                        thiscustomer.DDID = fourthlevelthing.Value;


                                }

                            }

                        }
                    }


                }

                string filetest = gjchurn.type;

                


                //Gets features from geojson and iterates through them for the relevant features
                dynamic thesefeatures = gjchurn.features;
                
                foreach(var item in thesefeatures.Children())
                {
                    string typeofvol = "";
                    SVOLinfo thissvol = new SVOLinfo();
                    foreach(var internalitem in item)
                    {
                        if(internalitem.Name =="properties")
                        {
                            foreach(var metaproperty in internalitem)
                            {
                                foreach (var property in metaproperty)
                                {
                                    //looks for relevant properties by name
                                    string value = property.Value.ToString();
                                    if (property.Name == "icaoID")
                                        thissvol.ICAOID = value;
                                    else if (property.Name == "sac")
                                    {
                                        thissvol.unfilteredsac = value;
                                        thissvol.sac = Convert.ToInt32(value, 16);
                                    }
                                    else if (property.Name == "sic")
                                    {
                                        thissvol.unfilteredsic = property.Value;
                                        thissvol.sic = Convert.ToInt32(value, 16);
                                    }
                                    else if (property.Name == "sid")
                                    {
                                        thissvol.unfilteredsid = property.Value;
                                        thissvol.sid = Convert.ToInt32(value, 16);
                                    }
                                    else if (property.Name == "featureType")
                                        typeofvol = property.Value;
                                    else if (property.Name == "volumeName")
                                        thissvol.VolumeName = property.Value;
                                    //assign values to customer object
                                }
                            }


                        }


                    }
                    if(typeofvol=="service")
                        thislist.Add(thissvol);


                }

                thiscustomer.SVOLS = thislist;
                listofcustomers.Add(thiscustomer);

            }
            for(int citer=0; citer <listofcustomers.Count; citer++)
            {
                for(int svoliter =0; svoliter < listofcustomers[citer].SVOLS.Count; svoliter++)
                {
                    listofcustomers[citer].SVOLS[svoliter].multicast = "233." + listofcustomers[citer].SVOLS[svoliter].sac.ToString() + "." + listofcustomers[citer].SVOLS[svoliter].sic.ToString() + "." + listofcustomers[citer].SVOLS[svoliter].sid.ToString();



                }


            }

        }

        //processes xml file for which messages are going through
        static void ProcessXMLfile(string xmlname)
        {
            //Populates list of CAT messages currently being investigated.
            List<string> listoftypes = new List<string>();
            string[] input = { "CAT238_SrvPred", "CAT025_Stats", "CAT253_TLE", "CAT247_Version", "CAT021", "CAT025_Status", "CAT023_ESR", "CAT238_SVolDef", "CAT023_Stats", "CAT023_Status" };
            listoftypes.AddRange(input);

            bool foundavol = false;
            string thismulticast = "";
            bool ismulticast = false;
            //Reads XML file as raw text.
            using (var fs = File.OpenRead(xmlname))
            using (var reader = new StreamReader(fs))
               
                while (!reader.EndOfStream)
                {

                    var line = reader.ReadLine();

                    //Signifies a multicast address.  Used here to show the start of a service volume, however that point is not the only point where this if statement is true.
                    if (line.Contains("<IP>") && !ismulticast)
                    {
                        ismulticast = true;
                        thismulticast = line;
                    }

                    //Searches through list of customers and SVols for pre built matching multicast.  NOTE: currently this is the way the program handles the fact that other lines have '<IP>'.
                    if (ismulticast && !foundavol)
                    {
                        bool thisisit = false;
                        //search through svol multicast IP's
                        for (int citer = 0; citer < listofcustomers.Count; citer++)
                        {

                            for (int svoliter = 0; svoliter < listofcustomers[citer].SVOLS.Count; svoliter++)
                            {
                                if (line.Contains(listofcustomers[citer].SVOLS[svoliter].multicast))
                                {
                                    foundavol = true;
                                    thisisit = true;
                                    break;
                                    //check to see if SVOL is created, if not create it


                                }


                            }
                            if (thisisit)
                                break;
                        }

                        //If the multicast cannot be matched, the program adds it to a list.  Currently needs work to figure out how to not add every ip in the xml.
                        if (!thisisit)
                        {
                            SVOLinfo newsvol = new SVOLinfo();
                            newsvol.SVOLID = line;
                            listofSVOLS.Add(newsvol);

                        }
                    }

                    //gets T/F value for CAT messages.
                    if (foundavol)
                    {
                        if (line.Contains("<Handle>"))
                        {
                            for (int citer = 0; citer < listofcustomers.Count; citer++)
                            {
                                for (int iter = 0; iter < listofcustomers[citer].SVOLS.Count; iter++)
                                {

                                    //The SVOLS match
                                    if (thismulticast.Contains(listofcustomers[citer].SVOLS[iter].multicast))
                                    {
                                        if (line.Contains("CAT238_SrvPred"))
                                            listofcustomers[citer].SVOLS[iter].has238SRVPRED = true;
                                        if (line.Contains("CAT025_Stats"))
                                            listofcustomers[citer].SVOLS[iter].has25STATS = true;
                                        if (line.Contains("CAT253_TLE"))
                                            listofcustomers[citer].SVOLS[iter].has253TLE = true;
                                        if (line.Contains("CAT247_Version"))
                                            listofcustomers[citer].SVOLS[iter].has247 = true;
                                        if (line.Contains("CAT021"))
                                            listofcustomers[citer].SVOLS[iter].has21 = true;
                                        if (line.Contains("CAT025_Status"))
                                            listofcustomers[citer].SVOLS[iter].has25STATUS = true;
                                        if (line.Contains("CAT023_ESR"))
                                            listofcustomers[citer].SVOLS[iter].has23ESR = true;
                                        if (line.Contains("CAT238_SVolDef"))
                                            listofcustomers[citer].SVOLS[iter].has238SVOLDEF = true;
                                        if (line.Contains("CAT023_Stats"))
                                            listofcustomers[citer].SVOLS[iter].has23STATS = true;
                                        if (line.Contains("CAT023_Status"))
                                            listofcustomers[citer].SVOLS[iter].has23STATUS = true;

                                        break;


                                    }


                                }

                            }





                        }
                        //currently matches with first port available.  Needs updating.
                        else if (line.Contains("<Port>"))
                        {

                            for (int citer = 0; citer < listofcustomers.Count; citer++)
                            {
                                for (int iter = 0; iter < listofcustomers[citer].SVOLS.Count; iter++)
                                {

                                    //The SVOLS match
                                    if (thismulticast.Contains(listofcustomers[citer].SVOLS[iter].multicast))
                                    {
                                        string procline = Regex.Match(line, @"\d+").Value;
                                        int port = Int32.Parse(procline);
                                        listofcustomers[citer].SVOLS[iter].port = port;
                                        break;




                                    }


                                }

                            }


                        }
                    }

                    //Signifies the end of a service volume
                    if (line.Contains("</InputMulticast>"))
                    {
                        ismulticast = false;
                        foundavol = false;
                    }

                }



        }

        //Prints out built customer list by service volume
        static void printexcelfile(string excelanme, string xmlname)
        {

            string[] toprow = { "Customer Name", "SVol name", "Svol Multicast", "sac", "sic", "sid", "Cat023 Enabled", "247", "238 Svoldef", "238 Service Prediction", "253 TLE", "33", "21", "23 Stats", "23 ESR", "23 status", "25 status", "25 stats", "Volume Name", "Port" };

            Excel.Application reportdoc = new Excel.Application();
            reportdoc.DisplayAlerts = false;
            //sourcedoc.Workbooks.Open(excelanme);
            Excel.Workbook reportbook = reportdoc.Workbooks.Add();
            Excel.Worksheet reportsheet = reportbook.ActiveSheet;
            Excel.Range movecell = reportsheet.Cells[1, 1];
            for(int topcount =1; topcount < toprow.Length+1; topcount++)
            {
                movecell = reportsheet.Cells[1, topcount];
                movecell.Value = toprow[topcount - 1];

            }

            //finds the column number of the next blank cell in the first column.  Should be 2.
            int rownum=Exceltools.nextblankcelllocation(reportsheet, 1,1);

            //defines Sets range object for iteration
            movecell = reportsheet.Cells[rownum, 1];
            
            //iterates through customer list
            for(int citer=0; citer < listofcustomers.Count; citer++)
            {
                //iterates through the service volumes each customer has
                for(int svoliter=0; svoliter < listofcustomers[citer].SVOLS.Count; svoliter++)
                {
                    //iterates across columns printing out relevant information.  NOTE: This can be done better.  It works now, but it could be better.
                    for(int colnum=1; colnum < 21;colnum++)
                    {
                        movecell = reportsheet.Cells[rownum, colnum];

                        //There is a more efficient way to do this, just haven't implemented.  Having the CAT messages print true if true and empty if not was a purposeful decision.
                        switch (colnum)
                        {
                            case 1:
                                movecell.Value = listofcustomers[citer].Name;
                                break;
                            case 2:
                                movecell.Value = listofcustomers[citer].SVOLS[svoliter].ICAOID;
                                break;
                            case 3:
                                movecell.Value = listofcustomers[citer].SVOLS[svoliter].multicast;
                                break;
                            case 4:
                                movecell.Value = listofcustomers[citer].SVOLS[svoliter].unfilteredsac;
                                break;
                            case 5:
                                movecell.Value = listofcustomers[citer].SVOLS[svoliter].unfilteredsic;
                                break;
                            case 6:
                                movecell.Value = listofcustomers[citer].SVOLS[svoliter].unfilteredsid;
                                break;
                            case 7:
                                movecell.Value = listofcustomers[citer].CAT023enabled;
                                break;
                            case 8:
                                if (listofcustomers[citer].SVOLS[svoliter].has247)
                                {
                                    movecell.Value = "TRUE";
                                }
                                break;
                            case 9:
                                if (listofcustomers[citer].SVOLS[svoliter].has238SVOLDEF)
                                {
                                    movecell.Value = "TRUE";
                                }
                                break;
                            case 10:
                                if (listofcustomers[citer].SVOLS[svoliter].has238SRVPRED)
                                {
                                    movecell.Value = "TRUE";
                                }
                                break;
                            case 11:
                                if (listofcustomers[citer].SVOLS[svoliter].has253TLE)
                                {
                                    movecell.Value = "TRUE";
                                }
                                break;
                            case 12:
                                if (listofcustomers[citer].SVOLS[svoliter].has33)
                                {
                                    movecell.Value = "TRUE";
                                }
                                break;
                            case 13:
                                if (listofcustomers[citer].SVOLS[svoliter].has21)
                                {
                                    movecell.Value = "TRUE";
                                }
                                break;
                            case 14:
                                if (listofcustomers[citer].SVOLS[svoliter].has23STATS)
                                {
                                    movecell.Value = "TRUE";
                                }
                                break;
                            case 15:
                                if (listofcustomers[citer].SVOLS[svoliter].has23ESR)
                                {
                                    movecell.Value = "TRUE";
                                }
                                break;
                            case 16:
                                if (listofcustomers[citer].SVOLS[svoliter].has23STATUS)
                                {
                                    movecell.Value = "TRUE";
                                }
                                break;
                            case 17:
                                if (listofcustomers[citer].SVOLS[svoliter].has25STATUS)
                                {
                                    movecell.Value = "TRUE";
                                }
                                break;
                            case 18:
                                if (listofcustomers[citer].SVOLS[svoliter].has25STATS)
                                {
                                    movecell.Value = "TRUE";
                                }
                                break;
                            case 19:
                                movecell.Value = listofcustomers[citer].SVOLS[svoliter].VolumeName;
                                break;
                            case 20:
                                movecell.Value = listofcustomers[citer].SVOLS[svoliter].port;
                                break;

                        }
                       
                    }


                    rownum++;
                    movecell = reportsheet.Cells[rownum, 1];
                }


            }

            //Saves excel file, then closes the .NET excel object.
            string saveplace = folderloc + @"/CAT Report";
            reportbook.SaveAs(saveplace);
            reportbook.Close(0);
            reportdoc.Quit();

        }


       
    }
}
