<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.2.2-Bonn" labelsEnabled="0" simplifyDrawingHints="1" simplifyMaxScale="1" readOnly="0" hasScaleBasedVisibilityFlag="0" minScale="1e+8" simplifyLocal="1" simplifyDrawingTol="1" simplifyAlgorithm="0" maxScale="0">
  <renderer-v2 forceraster="0" type="RuleRenderer" enableorderby="0" symbollevels="1">
    <rules key="{f45be026-dc4f-44fa-8d51-8045a71ee15a}">
      <rule label="atlas billing" symbol="0" filter=" $id =   @atlas_featureid AND&quot;featureType&quot; = 'billing'" key="{5f20ac24-6c15-4f8b-af31-2a997574de83}"/>
      <rule label="atlas service" symbol="1" filter=" $id = @atlas_featureid AND &quot;featureType&quot; = 'service'" key="{e32254db-6c56-4cae-894f-d8ec9f823e19}"/>
    </rules>
    <symbols>
      <symbol alpha="1" type="fill" name="0" clip_to_extent="1">
        <layer locked="0" enabled="1" class="SimpleFill" pass="0">
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="145,255,45,170" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.26" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" type="fill" name="1" clip_to_extent="1">
        <layer locked="0" enabled="1" class="SimpleFill" pass="0">
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="0,250,254,156" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.26" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <customproperties>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory backgroundAlpha="255" scaleDependency="Area" minScaleDenominator="0" diagramOrientation="Up" barWidth="5" enabled="0" minimumSize="0" penColor="#000000" penWidth="0" labelPlacementMethod="XHeight" opacity="1" penAlpha="255" maxScaleDenominator="1e+8" height="15" lineSizeScale="3x:0,0,0,0,0,0" scaleBasedVisibility="0" width="15" rotationOffset="270" backgroundColor="#ffffff" sizeType="MM" lineSizeType="MM" sizeScale="3x:0,0,0,0,0,0">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute label="" field="" color="#000000"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" linePlacementFlags="18" zIndex="0" placement="1" priority="0" showAll="1" dist="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="featureVersion">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="featureEffectiveDate">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="featureBirthDate">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="featureEndDate">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="customerID">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="volumeName">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="icaoID">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sac">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sic">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="featureID">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="featureType">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="serviceFeatureProperties">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="geometryType">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="centerPoint">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="floorFeet">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ceilingFeet">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="billingFeatureProperties">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" index="0" name=""/>
    <alias field="featureVersion" index="1" name=""/>
    <alias field="featureEffectiveDate" index="2" name=""/>
    <alias field="featureBirthDate" index="3" name=""/>
    <alias field="featureEndDate" index="4" name=""/>
    <alias field="customerID" index="5" name=""/>
    <alias field="volumeName" index="6" name=""/>
    <alias field="icaoID" index="7" name=""/>
    <alias field="sac" index="8" name=""/>
    <alias field="sic" index="9" name=""/>
    <alias field="sid" index="10" name=""/>
    <alias field="featureID" index="11" name=""/>
    <alias field="featureType" index="12" name=""/>
    <alias field="serviceFeatureProperties" index="13" name=""/>
    <alias field="geometryType" index="14" name=""/>
    <alias field="centerPoint" index="15" name=""/>
    <alias field="floorFeet" index="16" name=""/>
    <alias field="ceilingFeet" index="17" name=""/>
    <alias field="billingFeatureProperties" index="18" name=""/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="id" expression="" applyOnUpdate="0"/>
    <default field="featureVersion" expression="" applyOnUpdate="0"/>
    <default field="featureEffectiveDate" expression="" applyOnUpdate="0"/>
    <default field="featureBirthDate" expression="" applyOnUpdate="0"/>
    <default field="featureEndDate" expression="" applyOnUpdate="0"/>
    <default field="customerID" expression="" applyOnUpdate="0"/>
    <default field="volumeName" expression="" applyOnUpdate="0"/>
    <default field="icaoID" expression="" applyOnUpdate="0"/>
    <default field="sac" expression="" applyOnUpdate="0"/>
    <default field="sic" expression="" applyOnUpdate="0"/>
    <default field="sid" expression="" applyOnUpdate="0"/>
    <default field="featureID" expression="" applyOnUpdate="0"/>
    <default field="featureType" expression="" applyOnUpdate="0"/>
    <default field="serviceFeatureProperties" expression="" applyOnUpdate="0"/>
    <default field="geometryType" expression="" applyOnUpdate="0"/>
    <default field="centerPoint" expression="" applyOnUpdate="0"/>
    <default field="floorFeet" expression="" applyOnUpdate="0"/>
    <default field="ceilingFeet" expression="" applyOnUpdate="0"/>
    <default field="billingFeatureProperties" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint unique_strength="0" field="id" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="featureVersion" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="featureEffectiveDate" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="featureBirthDate" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="featureEndDate" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="customerID" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="volumeName" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="icaoID" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="sac" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="sic" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="sid" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="featureID" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="featureType" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="serviceFeatureProperties" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="geometryType" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="centerPoint" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="floorFeet" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="ceilingFeet" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="billingFeatureProperties" exp_strength="0" notnull_strength="0" constraints="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id" exp="" desc=""/>
    <constraint field="featureVersion" exp="" desc=""/>
    <constraint field="featureEffectiveDate" exp="" desc=""/>
    <constraint field="featureBirthDate" exp="" desc=""/>
    <constraint field="featureEndDate" exp="" desc=""/>
    <constraint field="customerID" exp="" desc=""/>
    <constraint field="volumeName" exp="" desc=""/>
    <constraint field="icaoID" exp="" desc=""/>
    <constraint field="sac" exp="" desc=""/>
    <constraint field="sic" exp="" desc=""/>
    <constraint field="sid" exp="" desc=""/>
    <constraint field="featureID" exp="" desc=""/>
    <constraint field="featureType" exp="" desc=""/>
    <constraint field="serviceFeatureProperties" exp="" desc=""/>
    <constraint field="geometryType" exp="" desc=""/>
    <constraint field="centerPoint" exp="" desc=""/>
    <constraint field="floorFeet" exp="" desc=""/>
    <constraint field="ceilingFeet" exp="" desc=""/>
    <constraint field="billingFeatureProperties" exp="" desc=""/>
  </constraintExpressions>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column type="field" name="id" hidden="0" width="-1"/>
      <column type="field" name="featureVersion" hidden="0" width="-1"/>
      <column type="field" name="featureEffectiveDate" hidden="0" width="-1"/>
      <column type="field" name="featureBirthDate" hidden="0" width="-1"/>
      <column type="field" name="featureEndDate" hidden="0" width="-1"/>
      <column type="field" name="customerID" hidden="0" width="-1"/>
      <column type="field" name="volumeName" hidden="0" width="-1"/>
      <column type="field" name="icaoID" hidden="0" width="-1"/>
      <column type="field" name="sac" hidden="0" width="-1"/>
      <column type="field" name="sic" hidden="0" width="-1"/>
      <column type="field" name="sid" hidden="0" width="-1"/>
      <column type="field" name="featureID" hidden="0" width="-1"/>
      <column type="field" name="featureType" hidden="0" width="-1"/>
      <column type="field" name="serviceFeatureProperties" hidden="0" width="-1"/>
      <column type="field" name="geometryType" hidden="0" width="-1"/>
      <column type="field" name="centerPoint" hidden="0" width="-1"/>
      <column type="field" name="floorFeet" hidden="0" width="-1"/>
      <column type="field" name="ceilingFeet" hidden="0" width="-1"/>
      <column type="field" name="billingFeatureProperties" hidden="0" width="-1"/>
      <column type="actions" hidden="1" width="-1"/>
    </columns>
  </attributetableconfig>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="billingFeatureProperties" editable="1"/>
    <field name="ceilingFeet" editable="1"/>
    <field name="centerPoint" editable="1"/>
    <field name="customerID" editable="1"/>
    <field name="featureBirthDate" editable="1"/>
    <field name="featureEffectiveDate" editable="1"/>
    <field name="featureEndDate" editable="1"/>
    <field name="featureID" editable="1"/>
    <field name="featureType" editable="1"/>
    <field name="featureVersion" editable="1"/>
    <field name="floorFeet" editable="1"/>
    <field name="geometryType" editable="1"/>
    <field name="icaoID" editable="1"/>
    <field name="id" editable="1"/>
    <field name="sac" editable="1"/>
    <field name="serviceFeatureProperties" editable="1"/>
    <field name="sic" editable="1"/>
    <field name="sid" editable="1"/>
    <field name="volumeName" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="billingFeatureProperties" labelOnTop="0"/>
    <field name="ceilingFeet" labelOnTop="0"/>
    <field name="centerPoint" labelOnTop="0"/>
    <field name="customerID" labelOnTop="0"/>
    <field name="featureBirthDate" labelOnTop="0"/>
    <field name="featureEffectiveDate" labelOnTop="0"/>
    <field name="featureEndDate" labelOnTop="0"/>
    <field name="featureID" labelOnTop="0"/>
    <field name="featureType" labelOnTop="0"/>
    <field name="featureVersion" labelOnTop="0"/>
    <field name="floorFeet" labelOnTop="0"/>
    <field name="geometryType" labelOnTop="0"/>
    <field name="icaoID" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="sac" labelOnTop="0"/>
    <field name="serviceFeatureProperties" labelOnTop="0"/>
    <field name="sic" labelOnTop="0"/>
    <field name="sid" labelOnTop="0"/>
    <field name="volumeName" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <expressionfields/>
  <previewExpression>id</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
