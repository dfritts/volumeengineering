<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.2.2-Bonn" labelsEnabled="1" simplifyDrawingHints="1" simplifyMaxScale="1" readOnly="0" hasScaleBasedVisibilityFlag="0" minScale="1e+8" simplifyLocal="1" simplifyDrawingTol="1" simplifyAlgorithm="0" maxScale="0">
  <renderer-v2 forceraster="0" type="singleSymbol" enableorderby="0" symbollevels="0">
    <symbols>
      <symbol alpha="1" type="line" name="0" clip_to_extent="1">
        <layer locked="0" enabled="1" class="SimpleLine" pass="0">
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="114,155,111,160" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.26" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <labeling type="simple">
    <settings>
      <text-style fieldName="display" fontItalic="0" isExpression="0" textColor="255,255,255,255" previewBkgrdColor="#ffffff" blendMode="0" fontLetterSpacing="0" fontWeight="50" useSubstitutions="0" fontFamily="MS Shell Dlg 2" fontWordSpacing="0" namedStyle="Regular" fontSize="10" multilineHeight="1" textOpacity="1" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontSizeUnit="Point" fontUnderline="0" fontStrikeout="0" fontCapitals="0">
        <text-buffer bufferOpacity="1" bufferJoinStyle="128" bufferBlendMode="0" bufferDraw="0" bufferNoFill="1" bufferSizeUnits="MM" bufferColor="255,255,255,255" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferSize="1"/>
        <background shapeBorderWidth="0" shapeType="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeFillColor="255,255,255,255" shapeBorderColor="128,128,128,255" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeOpacity="1" shapeOffsetY="0" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeSizeX="0" shapeRotation="0" shapeRadiiUnit="MM" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeDraw="0" shapeOffsetUnit="MM" shapeOffsetX="0" shapeSizeUnit="MM" shapeSizeType="0" shapeRotationType="0" shapeBorderWidthUnit="MM" shapeSizeY="0" shapeRadiiX="0" shapeRadiiY="0" shapeBlendMode="0" shapeSVGFile="" shapeJoinStyle="64"/>
        <shadow shadowOffsetAngle="135" shadowRadius="1.5" shadowRadiusUnit="MM" shadowScale="100" shadowDraw="0" shadowColor="0,0,0,255" shadowOffsetUnit="MM" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowBlendMode="6" shadowOpacity="0.7" shadowUnder="0" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowOffsetGlobal="1"/>
        <substitutions/>
      </text-style>
      <text-format leftDirectionSymbol="&lt;" rightDirectionSymbol=">" reverseDirectionSymbol="0" plussign="0" formatNumbers="0" decimals="3" wrapChar="" multilineAlign="4294967295" addDirectionSymbol="0" placeDirectionSymbol="0"/>
      <placement centroidWhole="0" offsetUnits="MM" repeatDistanceUnits="MM" distUnits="MM" quadOffset="4" preserveRotation="1" priority="5" dist="0" placement="2" repeatDistance="0" fitInPolygonOnly="0" xOffset="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" offsetType="0" placementFlags="10" distMapUnitScale="3x:0,0,0,0,0,0" centroidInside="0" maxCurvedCharAngleIn="25" yOffset="0" rotationAngle="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" maxCurvedCharAngleOut="-25"/>
      <rendering labelPerPart="0" minFeatureSize="0" fontLimitPixelSize="0" displayAll="0" limitNumLabels="0" scaleVisibility="0" obstacle="1" scaleMin="0" obstacleType="0" fontMaxPixelSize="10000" drawLabels="1" zIndex="0" fontMinPixelSize="3" upsidedownLabels="0" mergeLines="0" obstacleFactor="1" scaleMax="0" maxNumLabels="2000"/>
      <dd_properties>
        <Option type="Map">
          <Option value="" type="QString" name="name"/>
          <Option name="properties"/>
          <Option value="collection" type="QString" name="type"/>
        </Option>
      </dd_properties>
    </settings>
  </labeling>
  <customproperties>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory backgroundAlpha="255" scaleDependency="Area" minScaleDenominator="0" diagramOrientation="Up" barWidth="5" enabled="0" minimumSize="0" penColor="#000000" penWidth="0" labelPlacementMethod="XHeight" opacity="1" penAlpha="255" maxScaleDenominator="1e+8" height="15" lineSizeScale="3x:0,0,0,0,0,0" scaleBasedVisibility="0" width="15" rotationOffset="270" backgroundColor="#ffffff" sizeType="MM" lineSizeType="MM" sizeScale="3x:0,0,0,0,0,0">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute label="" field="" color="#000000"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" linePlacementFlags="18" zIndex="0" placement="2" priority="0" showAll="1" dist="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <fieldConfiguration>
    <field name="recnum">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="degrees">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="direction">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="display">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="scalerank">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="dd">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="recnum" index="0" name=""/>
    <alias field="degrees" index="1" name=""/>
    <alias field="direction" index="2" name=""/>
    <alias field="display" index="3" name=""/>
    <alias field="scalerank" index="4" name=""/>
    <alias field="dd" index="5" name=""/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="recnum" expression="" applyOnUpdate="0"/>
    <default field="degrees" expression="" applyOnUpdate="0"/>
    <default field="direction" expression="" applyOnUpdate="0"/>
    <default field="display" expression="" applyOnUpdate="0"/>
    <default field="scalerank" expression="" applyOnUpdate="0"/>
    <default field="dd" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint unique_strength="0" field="recnum" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="degrees" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="direction" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="display" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="scalerank" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="dd" exp_strength="0" notnull_strength="0" constraints="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="recnum" exp="" desc=""/>
    <constraint field="degrees" exp="" desc=""/>
    <constraint field="direction" exp="" desc=""/>
    <constraint field="display" exp="" desc=""/>
    <constraint field="scalerank" exp="" desc=""/>
    <constraint field="dd" exp="" desc=""/>
  </constraintExpressions>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column type="field" name="recnum" hidden="0" width="-1"/>
      <column type="field" name="degrees" hidden="0" width="-1"/>
      <column type="field" name="direction" hidden="0" width="-1"/>
      <column type="field" name="display" hidden="0" width="-1"/>
      <column type="field" name="scalerank" hidden="0" width="-1"/>
      <column type="field" name="dd" hidden="0" width="-1"/>
      <column type="actions" hidden="1" width="-1"/>
    </columns>
  </attributetableconfig>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="dd" editable="1"/>
    <field name="degrees" editable="1"/>
    <field name="direction" editable="1"/>
    <field name="display" editable="1"/>
    <field name="recnum" editable="1"/>
    <field name="scalerank" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="dd" labelOnTop="0"/>
    <field name="degrees" labelOnTop="0"/>
    <field name="direction" labelOnTop="0"/>
    <field name="display" labelOnTop="0"/>
    <field name="recnum" labelOnTop="0"/>
    <field name="scalerank" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <expressionfields/>
  <previewExpression>recnum</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
