<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.2.2-Bonn" labelsEnabled="1" simplifyDrawingHints="1" simplifyMaxScale="1" readOnly="0" hasScaleBasedVisibilityFlag="0" minScale="1e+8" simplifyLocal="1" simplifyDrawingTol="1" simplifyAlgorithm="0" maxScale="0">
  <renderer-v2 forceraster="0" type="singleSymbol" enableorderby="0" symbollevels="0">
    <symbols>
      <symbol alpha="1" type="fill" name="0" clip_to_extent="1">
        <layer locked="0" enabled="1" class="SimpleFill" pass="0">
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="164,113,88,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="162,162,162,185" k="outline_color"/>
          <prop v="dash" k="outline_style"/>
          <prop v="0.46" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="no" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <labeling type="simple">
    <settings>
      <text-style fieldName="ICAOCODE || '\n' || FIRname" fontItalic="0" isExpression="1" textColor="231,231,231,255" previewBkgrdColor="#ffffff" blendMode="0" fontLetterSpacing="0" fontWeight="50" useSubstitutions="0" fontFamily="MS Shell Dlg 2" fontWordSpacing="0" namedStyle="Regular" fontSize="7" multilineHeight="1" textOpacity="1" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontSizeUnit="Point" fontUnderline="0" fontStrikeout="0" fontCapitals="0">
        <text-buffer bufferOpacity="1" bufferJoinStyle="128" bufferBlendMode="0" bufferDraw="0" bufferNoFill="1" bufferSizeUnits="MM" bufferColor="255,255,255,255" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferSize="1"/>
        <background shapeBorderWidth="0" shapeType="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeFillColor="255,255,255,255" shapeBorderColor="128,128,128,255" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeOpacity="1" shapeOffsetY="0" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeSizeX="0" shapeRotation="0" shapeRadiiUnit="MM" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeDraw="0" shapeOffsetUnit="MM" shapeOffsetX="0" shapeSizeUnit="MM" shapeSizeType="0" shapeRotationType="0" shapeBorderWidthUnit="MM" shapeSizeY="0" shapeRadiiX="0" shapeRadiiY="0" shapeBlendMode="0" shapeSVGFile="" shapeJoinStyle="64"/>
        <shadow shadowOffsetAngle="135" shadowRadius="1.5" shadowRadiusUnit="MM" shadowScale="100" shadowDraw="0" shadowColor="0,0,0,255" shadowOffsetUnit="MM" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowBlendMode="6" shadowOpacity="0.7" shadowUnder="0" shadowRadiusAlphaOnly="0" shadowOffsetDist="1" shadowOffsetGlobal="1"/>
        <substitutions/>
      </text-style>
      <text-format leftDirectionSymbol="&lt;" rightDirectionSymbol=">" reverseDirectionSymbol="0" plussign="0" formatNumbers="0" decimals="3" wrapChar="" multilineAlign="4294967295" addDirectionSymbol="0" placeDirectionSymbol="0"/>
      <placement centroidWhole="0" offsetUnits="MM" repeatDistanceUnits="MM" distUnits="MM" quadOffset="4" preserveRotation="1" priority="5" dist="0" placement="0" repeatDistance="0" fitInPolygonOnly="0" xOffset="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" offsetType="0" placementFlags="10" distMapUnitScale="3x:0,0,0,0,0,0" centroidInside="0" maxCurvedCharAngleIn="25" yOffset="0" rotationAngle="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" maxCurvedCharAngleOut="-25"/>
      <rendering labelPerPart="0" minFeatureSize="0" fontLimitPixelSize="0" displayAll="0" limitNumLabels="0" scaleVisibility="0" obstacle="1" scaleMin="0" obstacleType="0" fontMaxPixelSize="10000" drawLabels="1" zIndex="0" fontMinPixelSize="3" upsidedownLabels="0" mergeLines="0" obstacleFactor="1" scaleMax="0" maxNumLabels="2000"/>
      <dd_properties>
        <Option type="Map">
          <Option value="" type="QString" name="name"/>
          <Option name="properties"/>
          <Option value="collection" type="QString" name="type"/>
        </Option>
      </dd_properties>
    </settings>
  </labeling>
  <customproperties>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory backgroundAlpha="255" scaleDependency="Area" minScaleDenominator="0" diagramOrientation="Up" barWidth="5" enabled="0" minimumSize="0" penColor="#000000" penWidth="0" labelPlacementMethod="XHeight" opacity="1" penAlpha="255" maxScaleDenominator="1e+8" height="15" lineSizeScale="3x:0,0,0,0,0,0" scaleBasedVisibility="0" width="15" rotationOffset="270" backgroundColor="#ffffff" sizeType="MM" lineSizeType="MM" sizeScale="3x:0,0,0,0,0,0">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute label="" field="" color="#000000"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" linePlacementFlags="18" zIndex="0" placement="1" priority="0" showAll="1" dist="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <fieldConfiguration>
    <field name="id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="featureVersion">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="volumeName">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sic">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ceilingFeet">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="icaoID">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="geometryType">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="featureEndDate">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="featureID">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="floorFeet">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sac">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="featureType">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" index="0" name=""/>
    <alias field="featureVersion" index="1" name=""/>
    <alias field="sid" index="2" name=""/>
    <alias field="volumeName" index="3" name=""/>
    <alias field="sic" index="4" name=""/>
    <alias field="ceilingFeet" index="5" name=""/>
    <alias field="icaoID" index="6" name=""/>
    <alias field="geometryType" index="7" name=""/>
    <alias field="featureEndDate" index="8" name=""/>
    <alias field="featureID" index="9" name=""/>
    <alias field="floorFeet" index="10" name=""/>
    <alias field="sac" index="11" name=""/>
    <alias field="featureType" index="12" name=""/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="id" expression="" applyOnUpdate="0"/>
    <default field="featureVersion" expression="" applyOnUpdate="0"/>
    <default field="sid" expression="" applyOnUpdate="0"/>
    <default field="volumeName" expression="" applyOnUpdate="0"/>
    <default field="sic" expression="" applyOnUpdate="0"/>
    <default field="ceilingFeet" expression="" applyOnUpdate="0"/>
    <default field="icaoID" expression="" applyOnUpdate="0"/>
    <default field="geometryType" expression="" applyOnUpdate="0"/>
    <default field="featureEndDate" expression="" applyOnUpdate="0"/>
    <default field="featureID" expression="" applyOnUpdate="0"/>
    <default field="floorFeet" expression="" applyOnUpdate="0"/>
    <default field="sac" expression="" applyOnUpdate="0"/>
    <default field="featureType" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint unique_strength="0" field="id" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="featureVersion" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="sid" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="volumeName" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="sic" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="ceilingFeet" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="icaoID" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="geometryType" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="featureEndDate" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="featureID" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="floorFeet" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="sac" exp_strength="0" notnull_strength="0" constraints="0"/>
    <constraint unique_strength="0" field="featureType" exp_strength="0" notnull_strength="0" constraints="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id" exp="" desc=""/>
    <constraint field="featureVersion" exp="" desc=""/>
    <constraint field="sid" exp="" desc=""/>
    <constraint field="volumeName" exp="" desc=""/>
    <constraint field="sic" exp="" desc=""/>
    <constraint field="ceilingFeet" exp="" desc=""/>
    <constraint field="icaoID" exp="" desc=""/>
    <constraint field="geometryType" exp="" desc=""/>
    <constraint field="featureEndDate" exp="" desc=""/>
    <constraint field="featureID" exp="" desc=""/>
    <constraint field="floorFeet" exp="" desc=""/>
    <constraint field="sac" exp="" desc=""/>
    <constraint field="featureType" exp="" desc=""/>
  </constraintExpressions>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column type="actions" hidden="1" width="-1"/>
      <column type="field" name="id" hidden="0" width="-1"/>
      <column type="field" name="featureVersion" hidden="0" width="-1"/>
      <column type="field" name="sid" hidden="0" width="-1"/>
      <column type="field" name="volumeName" hidden="0" width="-1"/>
      <column type="field" name="sic" hidden="0" width="-1"/>
      <column type="field" name="ceilingFeet" hidden="0" width="-1"/>
      <column type="field" name="icaoID" hidden="0" width="-1"/>
      <column type="field" name="geometryType" hidden="0" width="-1"/>
      <column type="field" name="featureEndDate" hidden="0" width="-1"/>
      <column type="field" name="featureID" hidden="0" width="-1"/>
      <column type="field" name="floorFeet" hidden="0" width="-1"/>
      <column type="field" name="sac" hidden="0" width="-1"/>
      <column type="field" name="featureType" hidden="0" width="-1"/>
    </columns>
  </attributetableconfig>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="ANP_Region" editable="1"/>
    <field name="AREAsqkm" editable="1"/>
    <field name="CODE" editable="1"/>
    <field name="Country_So" editable="1"/>
    <field name="FIR_Lowe_1" editable="1"/>
    <field name="FIR_Lower" editable="1"/>
    <field name="FIR_NAME" editable="1"/>
    <field name="FIR_Uppe_1" editable="1"/>
    <field name="FIR_Upper" editable="1"/>
    <field name="FIRname" editable="1"/>
    <field name="HISTORIC" editable="1"/>
    <field name="ICAOCODE" editable="1"/>
    <field name="NOM_COMP" editable="1"/>
    <field name="Number_lim" editable="1"/>
    <field name="PERIMEkm" editable="1"/>
    <field name="REMARKS" editable="1"/>
    <field name="REMARKS2" editable="1"/>
    <field name="RESP" editable="1"/>
    <field name="Type" editable="1"/>
    <field name="UIR_Lowe_1" editable="1"/>
    <field name="UIR_Lower" editable="1"/>
    <field name="UIR_Uppe_1" editable="1"/>
    <field name="UIR_Upper" editable="1"/>
    <field name="ceilingFeet" editable="1"/>
    <field name="centlat" editable="1"/>
    <field name="centlong" editable="1"/>
    <field name="featureEndDate" editable="1"/>
    <field name="featureID" editable="1"/>
    <field name="featureType" editable="1"/>
    <field name="featureVersion" editable="1"/>
    <field name="floorFeet" editable="1"/>
    <field name="geometryType" editable="1"/>
    <field name="icaoID" editable="1"/>
    <field name="id" editable="1"/>
    <field name="limit" editable="1"/>
    <field name="sac" editable="1"/>
    <field name="sic" editable="1"/>
    <field name="sid" editable="1"/>
    <field name="volumeName" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="ANP_Region" labelOnTop="0"/>
    <field name="AREAsqkm" labelOnTop="0"/>
    <field name="CODE" labelOnTop="0"/>
    <field name="Country_So" labelOnTop="0"/>
    <field name="FIR_Lowe_1" labelOnTop="0"/>
    <field name="FIR_Lower" labelOnTop="0"/>
    <field name="FIR_NAME" labelOnTop="0"/>
    <field name="FIR_Uppe_1" labelOnTop="0"/>
    <field name="FIR_Upper" labelOnTop="0"/>
    <field name="FIRname" labelOnTop="0"/>
    <field name="HISTORIC" labelOnTop="0"/>
    <field name="ICAOCODE" labelOnTop="0"/>
    <field name="NOM_COMP" labelOnTop="0"/>
    <field name="Number_lim" labelOnTop="0"/>
    <field name="PERIMEkm" labelOnTop="0"/>
    <field name="REMARKS" labelOnTop="0"/>
    <field name="REMARKS2" labelOnTop="0"/>
    <field name="RESP" labelOnTop="0"/>
    <field name="Type" labelOnTop="0"/>
    <field name="UIR_Lowe_1" labelOnTop="0"/>
    <field name="UIR_Lower" labelOnTop="0"/>
    <field name="UIR_Uppe_1" labelOnTop="0"/>
    <field name="UIR_Upper" labelOnTop="0"/>
    <field name="ceilingFeet" labelOnTop="0"/>
    <field name="centlat" labelOnTop="0"/>
    <field name="centlong" labelOnTop="0"/>
    <field name="featureEndDate" labelOnTop="0"/>
    <field name="featureID" labelOnTop="0"/>
    <field name="featureType" labelOnTop="0"/>
    <field name="featureVersion" labelOnTop="0"/>
    <field name="floorFeet" labelOnTop="0"/>
    <field name="geometryType" labelOnTop="0"/>
    <field name="icaoID" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="limit" labelOnTop="0"/>
    <field name="sac" labelOnTop="0"/>
    <field name="sic" labelOnTop="0"/>
    <field name="sid" labelOnTop="0"/>
    <field name="volumeName" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <expressionfields/>
  <previewExpression>FIRname</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
