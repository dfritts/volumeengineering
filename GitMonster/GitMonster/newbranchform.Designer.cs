﻿namespace GitMonster
{
    partial class newbranchform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.newBranchBox = new System.Windows.Forms.TextBox();
            this.createNewBranchButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // newBranchBox
            // 
            this.newBranchBox.Location = new System.Drawing.Point(12, 84);
            this.newBranchBox.Name = "newBranchBox";
            this.newBranchBox.Size = new System.Drawing.Size(434, 26);
            this.newBranchBox.TabIndex = 0;
            this.newBranchBox.Text = "Enter new Branch Name Here";
            // 
            // createNewBranchButton
            // 
            this.createNewBranchButton.Location = new System.Drawing.Point(174, 147);
            this.createNewBranchButton.Name = "createNewBranchButton";
            this.createNewBranchButton.Size = new System.Drawing.Size(75, 37);
            this.createNewBranchButton.TabIndex = 1;
            this.createNewBranchButton.Text = "Create";
            this.createNewBranchButton.UseVisualStyleBackColor = true;
            this.createNewBranchButton.Click += new System.EventHandler(this.createNewBranchButton_Click);
            // 
            // newbranchform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 250);
            this.Controls.Add(this.createNewBranchButton);
            this.Controls.Add(this.newBranchBox);
            this.Name = "newbranchform";
            this.Text = "newbranchform";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox newBranchBox;
        private System.Windows.Forms.Button createNewBranchButton;
    }
}