﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GitMonster
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //Click the button go here
        private void goButton_Click(object sender, EventArgs e)
        {
            //Gets user inpouts when you click the button
            string folderloc = repoBox.Text;
            string command = cmdBox.Text;
            string number = numberBox.Text;
            string typetobechanged = typeBox.Text;
            string branch = "";


            if (!typetobechanged.Contains("Select") && number!="")
            {
                branch = typetobechanged + "-" + number + "-";
                Program.runGitCommand(folderloc, command, branch);
            }  
            else
                MessageBox.Show("Please select either CCR or REL and make sure there is a number in the appropriate box");


            


        }
    }
}
