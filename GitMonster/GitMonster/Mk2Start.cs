﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GitMonster
{
    public partial class Mk2Start : Form
    {
        public Mk2Start()
        {
            InitializeComponent();
        }

        private void runButton_Click(object sender, EventArgs e)
        {
            string folderloc = folderBox.Text;
            string type = typeBox.Text;
            DateTime dueDate = dueDatePicker.Value;

            

            Dictionary<int,string> sysUXinput = new Dictionary<int, string>();

            //Checks for "valid" folder location
            if (folderBox.Text != "Enter Folder Location")
                Program.runcommandmk2(folderloc, type, dueDate, sysUXinput);
            else
                MessageBox.Show("Please Enter Valid Folder Location");

        }
    }
}
