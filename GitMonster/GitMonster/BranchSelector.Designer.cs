﻿namespace GitMonster
{
    partial class BranchSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.branchBox = new System.Windows.Forms.ComboBox();
            this.branchSelectorButton = new System.Windows.Forms.Button();
            this.newBranchButton = new System.Windows.Forms.Button();
            this.skipButton = new System.Windows.Forms.Button();
            this.branchLabel = new System.Windows.Forms.Label();
            this.infoLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // branchBox
            // 
            this.branchBox.FormattingEnabled = true;
            this.branchBox.Location = new System.Drawing.Point(12, 99);
            this.branchBox.Name = "branchBox";
            this.branchBox.Size = new System.Drawing.Size(435, 28);
            this.branchBox.TabIndex = 0;
            this.branchBox.SelectedIndexChanged += new System.EventHandler(this.branchBox_SelectedIndexChanged);
            // 
            // branchSelectorButton
            // 
            this.branchSelectorButton.Location = new System.Drawing.Point(101, 152);
            this.branchSelectorButton.Name = "branchSelectorButton";
            this.branchSelectorButton.Size = new System.Drawing.Size(225, 31);
            this.branchSelectorButton.TabIndex = 1;
            this.branchSelectorButton.Text = "Found it!";
            this.branchSelectorButton.UseVisualStyleBackColor = true;
            this.branchSelectorButton.Click += new System.EventHandler(this.branchSelectorButton_Click);
            // 
            // newBranchButton
            // 
            this.newBranchButton.Location = new System.Drawing.Point(101, 204);
            this.newBranchButton.Name = "newBranchButton";
            this.newBranchButton.Size = new System.Drawing.Size(225, 31);
            this.newBranchButton.TabIndex = 2;
            this.newBranchButton.Text = "Create New Branch";
            this.newBranchButton.UseVisualStyleBackColor = true;
            this.newBranchButton.Click += new System.EventHandler(this.newBranchButton_Click);
            // 
            // skipButton
            // 
            this.skipButton.Location = new System.Drawing.Point(101, 255);
            this.skipButton.Name = "skipButton";
            this.skipButton.Size = new System.Drawing.Size(225, 31);
            this.skipButton.TabIndex = 3;
            this.skipButton.Text = "Skip this Repo";
            this.skipButton.UseVisualStyleBackColor = true;
            this.skipButton.Click += new System.EventHandler(this.skipButton_Click);
            // 
            // branchLabel
            // 
            this.branchLabel.AutoSize = true;
            this.branchLabel.Location = new System.Drawing.Point(12, 47);
            this.branchLabel.Name = "branchLabel";
            this.branchLabel.Size = new System.Drawing.Size(51, 20);
            this.branchLabel.TabIndex = 4;
            this.branchLabel.Text = "label1";
            // 
            // infoLabel
            // 
            this.infoLabel.AutoSize = true;
            this.infoLabel.Location = new System.Drawing.Point(8, 9);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Size = new System.Drawing.Size(419, 20);
            this.infoLabel.TabIndex = 5;
            this.infoLabel.Text = "Could not automatically match input with existing branches";
            // 
            // BranchSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 290);
            this.Controls.Add(this.infoLabel);
            this.Controls.Add(this.branchLabel);
            this.Controls.Add(this.skipButton);
            this.Controls.Add(this.newBranchButton);
            this.Controls.Add(this.branchSelectorButton);
            this.Controls.Add(this.branchBox);
            this.Name = "BranchSelector";
            this.Text = "BranchSelector";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox branchBox;
        private System.Windows.Forms.Button branchSelectorButton;
        private System.Windows.Forms.Button newBranchButton;
        private System.Windows.Forms.Button skipButton;
        private System.Windows.Forms.Label branchLabel;
        private System.Windows.Forms.Label infoLabel;
    }
}