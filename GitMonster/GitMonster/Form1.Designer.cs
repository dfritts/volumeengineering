﻿namespace GitMonster
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdBox = new System.Windows.Forms.TextBox();
            this.goButton = new System.Windows.Forms.Button();
            this.repoBox = new System.Windows.Forms.TextBox();
            this.numberBox = new System.Windows.Forms.TextBox();
            this.typeBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // cmdBox
            // 
            this.cmdBox.Location = new System.Drawing.Point(82, 330);
            this.cmdBox.Name = "cmdBox";
            this.cmdBox.Size = new System.Drawing.Size(203, 26);
            this.cmdBox.TabIndex = 0;
            this.cmdBox.Text = "Enter Git Command Here";
            // 
            // goButton
            // 
            this.goButton.Location = new System.Drawing.Point(136, 371);
            this.goButton.Name = "goButton";
            this.goButton.Size = new System.Drawing.Size(81, 31);
            this.goButton.TabIndex = 1;
            this.goButton.Text = "Run Git";
            this.goButton.UseVisualStyleBackColor = true;
            this.goButton.Click += new System.EventHandler(this.goButton_Click);
            // 
            // repoBox
            // 
            this.repoBox.Location = new System.Drawing.Point(82, 211);
            this.repoBox.Name = "repoBox";
            this.repoBox.Size = new System.Drawing.Size(203, 26);
            this.repoBox.TabIndex = 2;
            this.repoBox.Text = "Enter Local Repo Location";
            // 
            // numberBox
            // 
            this.numberBox.Location = new System.Drawing.Point(82, 274);
            this.numberBox.Name = "numberBox";
            this.numberBox.Size = new System.Drawing.Size(203, 26);
            this.numberBox.TabIndex = 3;
            this.numberBox.Text = "Enter Number";
            // 
            // typeBox
            // 
            this.typeBox.FormattingEnabled = true;
            this.typeBox.Items.AddRange(new object[] {
            "CCR",
            "REL"});
            this.typeBox.Location = new System.Drawing.Point(82, 144);
            this.typeBox.Name = "typeBox";
            this.typeBox.Size = new System.Drawing.Size(203, 28);
            this.typeBox.TabIndex = 4;
            this.typeBox.Text = "Select is Being Changed";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 450);
            this.Controls.Add(this.typeBox);
            this.Controls.Add(this.numberBox);
            this.Controls.Add(this.repoBox);
            this.Controls.Add(this.goButton);
            this.Controls.Add(this.cmdBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox cmdBox;
        private System.Windows.Forms.Button goButton;
        private System.Windows.Forms.TextBox repoBox;
        private System.Windows.Forms.TextBox numberBox;
        private System.Windows.Forms.ComboBox typeBox;
    }
}

