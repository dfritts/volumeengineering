﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GitMonster
{
    public partial class newbranchform : Form
    {
        public newbranchform(string thistype, string thisdate)
        {
            InitializeComponent();

            newBranchBox.Text = thistype + "-XXXX-" + thisdate;
        }

        private void createNewBranchButton_Click(object sender, EventArgs e)
        {

            string branchName = newBranchBox.Text;

            Program.CreateNewBranch(branchName);

            DialogResult = DialogResult.OK;
        }
    }
}
