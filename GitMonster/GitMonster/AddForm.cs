﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GitMonster
{
    public partial class AddForm : Form
    {
        static List<string> listoffiles = new List<string>();
        static string addthisfile = "";

        public AddForm(List<string> importList, string importBranch, string importSys)
        {
            InitializeComponent();

            sysLabel.Text = "For System: " + importSys;
            branchLabel.Text = "On Branch " + importBranch;


            listoffiles = importList;
            BindingSource bs = new BindingSource();
            bs.DataSource = listoffiles;

            fileListBox.DataSource = bs;

        }

        private void addButton_Click(object sender, EventArgs e)
        {
            string addText = addBox.Text;
            Program.AddThis(addText);

            DialogResult = DialogResult.OK;
        }


        private void fileListBox_DoubleClick(object sender, EventArgs e)
        {
            addthisfile = fileListBox.SelectedItem.ToString();

            addBox.Text = addBox.Text + addthisfile + " ";

        }

        private void donotAddButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;

        }
    }
}
