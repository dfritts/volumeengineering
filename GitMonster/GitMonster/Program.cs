﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Management;
using System.IO;
using System.Management.Automation;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;


namespace GitMonster
{
    static class Program
    {

        public static bool isadg = false, isapd = false, isabs = false, isdb = false, isthotherone = false, newbranch = false, skipthisrepo=true;
        public static string selectedbranch = "", Folderloc = "", System = "", newBranchName = "", thingstoAdd="", commitMessage="";



        [STAThread]
        static void Main()
        {
            //Sets form standards
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Mk2Start());

        }

        //Legacy code from Mk1 git user.  May update later to include this as an option
        public static void runGitCommand(string folderloc, string command, string branch)
        {
            //for testing
            branch = "GitMonster-Development";

            string gitCommand = "git ";

            Directory.SetCurrentDirectory(folderloc);

            using (PowerShell gitShell = PowerShell.Create())
            {

                //moves shell to desired repo location
                gitShell.AddScript(string.Format(@"cd {0}", folderloc));

                //command structures here
                //Always pull first
                //gitShell.AddScript(gitCommand + "pull");

                //Move to appropriate branch


                gitShell.AddScript(gitCommand + "branch -a");

                //Experiment to see if we can use tab in powershell through this application
                //SendKeys.Send("{TAB}");

                Collection<PSObject> outputs = gitShell.Invoke();
                foreach (PSObject outputthing in outputs)
                {
                    if (outputthing != null)
                    {
                        Console.WriteLine(outputthing);

                    }

                }
            }
            
            using (PowerShell gitShell = PowerShell.Create())
            {



                //puts in command here
                gitShell.AddScript(gitCommand + command);



                //for debug
            }
        }


        public static void runcommandmk2(string folderloc,string type, DateTime dueDate, Dictionary<int, string> sysUXinput)
        {
            //input from user form goes to method which returns a string array of affected systems.
            string[] sysAffected = syschooser(sysUXinput);

            //for testing changed from "git " to "nogit "
            string gitCommand = "git ";
            List<string> listofBranches = new List<string>();
            List<string> listofchangedfiles = new List<string>();
            string thisistheBranch = "";
            listofBranches.Add("Please Select the Correct Branch");

            //parse folderlocation for format

            folderloc = "'" + folderloc;

            //Format datetime to standard format
            string textdate = dueDate.ToString("dd'-'MMMM'-'yyyy");
            textdate = textdate.TrimStart('0');





            foreach (string system in sysAffected)
            {
                newbranch = false;
                skipthisrepo = true;
                thisistheBranch = "";
                using (PowerShell gitShell = PowerShell.Create())
                {
                    string placetogo = string.Format(@"cd " + folderloc + "/" + system + "'");
                    //moves shell to desired repo location
                    gitShell.AddScript(string.Format(@"cd " + folderloc + "/" + system + "'"));

                    
                    //Always pull first to make sure the repo is up to date
                    gitShell.AddScript(gitCommand + "pull");

                    gitShell.AddScript(gitCommand + "status");
                    Collection<PSObject> outputs = gitShell.Invoke();

                    //Looks throug branches
                    gitShell.AddScript(gitCommand + "branch -a");

                    //Experiment to see if we can use tab in powershell through this application
                    //SendKeys.Send("{TAB}");

                    Collection<PSObject> branchOutputs = gitShell.Invoke();
                    foreach (PSObject outputthing in branchOutputs)
                    {
                        if (outputthing != null)
                        {
                            //adds all branches to a list of strings for later use (if necessary)
                            string thisOutput = outputthing.ToString();
                            listofBranches.Add(thisOutput);

                            string locsys = system;

                            //Matches with branch based on type (CCR/REL) and date
                            

                            if (thisOutput.Contains(type) && thisOutput.Contains(textdate))
                            {
                                thisistheBranch = thisOutput;
                                skipthisrepo = false;
                                break;
                            }
                        }

                    }
                    branchOutputs.Clear();

                    //if thisisthebranch is blank, this returns 
                    if(thisistheBranch=="")
                        thisistheBranch = midbranchselection(thisistheBranch, listofBranches, system,type, textdate);

                    //clear list for next iteration
                    listofBranches.Clear();

                    if (!skipthisrepo)
                    {
                        thisistheBranch = thisistheBranch.TrimStart('*');
                        thisistheBranch = thisistheBranch.Trim();

                        //needs to add a step where it shows the user the automatically found branch

                        if (newbranch)
                        {
                            //if a new branch is needed the input from the form is used with checkout -b command which creates a new branch with the given name
                            thisistheBranch = newBranchName;
                            gitShell.AddScript(gitCommand + "checkout -b " + thisistheBranch);
                            gitShell.AddScript(gitCommand + "push --set-upstream origin " + thisistheBranch);
                        }
                        else
                        {
                            //checks out relevant branch
                            gitShell.AddScript(gitCommand + "checkout " + thisistheBranch);
                        }

                        //add commands here (add, commit, push)

                        //gets list of changed files which have not been staged
                        //gets existing files with changes
                        gitShell.AddScript(gitCommand + "diff --name-only");
                        Collection<PSObject> changefileoutputs = gitShell.Invoke();

                        //gets newly added files
                        gitShell.AddScript(gitCommand + "ls-files --others");
                        Collection<PSObject> newfileoutputs = gitShell.Invoke();

                        foreach(PSObject changedfile in changefileoutputs)
                        {
                            if(changedfile!=null)
                            {
                                listofchangedfiles.Add(changedfile.ToString());


                            }
                        }

                        foreach (PSObject newfile in newfileoutputs)
                        {
                            if (newfile != null)
                            {
                                listofchangedfiles.Add(newfile.ToString());


                            }
                        }

                        //gets user input for staging files
                        DialogResult addResult = MessageBox.Show("Are there any files you would like to stage?", "Add Query", MessageBoxButtons.YesNo);

                        if (addResult == DialogResult.Yes)
                        {
                            //gets things to add from user
                            AddForm Addthis = new AddForm(listofchangedfiles, thisistheBranch, system);
                            DialogResult Adding = Addthis.ShowDialog();
                            string testadd = gitCommand + "add " + thingstoAdd;
                            gitShell.AddScript(gitCommand + "add " + thingstoAdd);

                            //clear list for next iteration
                            listofchangedfiles.Clear();

                            DialogResult commitResult = MessageBox.Show("Are there any files you would like to commit?", "Commit Query", MessageBoxButtons.YesNo);

                            if (commitResult == DialogResult.Yes)
                            {
                                //Gets commit message from user
                                MessageForm ThisMessage = new MessageForm();
                                DialogResult Messaging = ThisMessage.ShowDialog();
                                string testscript = gitCommand + "commit -m\"" + commitMessage + "\"";
                                gitShell.AddScript(gitCommand + "commit -m\"" + commitMessage + "\"");

                                //Asks user if they are ready to push
                                DialogResult pushResult = MessageBox.Show("Are you ready to push your changes?", "Push Query", MessageBoxButtons.YesNo);

                                if(pushResult == DialogResult.Yes)
                                {
                                    gitShell.AddScript(gitCommand + "push");

                                }

                            }
                            else
                            { }

                            Collection<PSObject> newoutputs = gitShell.Invoke();
                        }
                        else
                        { }
                    }
                }

            }
            //close application
            Application.Exit();
        }



        static string midbranchselection(string thisistheBranch, List<string> listofBranches, string thissys, string thistype, string thisdate)
        {
            if (thisistheBranch == "")
            {
                //initialize the selector form
                //returns thisisthebranch with new branch name
                BranchSelector selectBranch = new BranchSelector(listofBranches, thissys, thistype, thisdate);
                DialogResult brnchrslt = selectBranch.ShowDialog();
                
                


                //thisbranchselected("");

                thisistheBranch = selectedbranch;

               

            }


            return thisistheBranch;
        }

        //returns an array of strings listing the repo names of the big four
        static string[] syschooser(Dictionary<int, string> sysUXinput)
        {
            string[] sysAffected = { "configabstest", "configadgtest", "Configapdtest", "configglobaltest" };
       
                //Logic to churn through and switch on appropriate booleans here.
                //NOTE: THIS MAY BE CHANGING FROM A STRING[] BASED LOGIC SYSTEM DEPENDING ON HOW WE ROLL WITH INPUTS
                //FURTHER NOTE: I'M THINKING THIS WILL BE A DICTIONARY BASED SYSTEM INSTEAD BASED ON UX

            

            return sysAffected;
        }

        //method to set global variable for selected branch from menu
        public static void thisbranchselected(string selection)
        {
            selectedbranch = selection;
            skipthisrepo = false;
            return;
        }

        //method to set global variabls for use if a new branch is to be created
        public static void CreateNewBranch(string branchName)
        {
            newbranch = true;
            newBranchName = branchName;
            skipthisrepo = false;
            return;
        }

        //method to set global string for things to add
        public static void AddThis(string addthing)
        {
            thingstoAdd = addthing;
            return;

        }

        //method to set global string for commit message
        public static void CommitMessage(string message)
        {
            commitMessage = message;
            return;

        }


    }
}
