﻿namespace GitMonster
{
    partial class AddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addLabel = new System.Windows.Forms.Label();
            this.addBox = new System.Windows.Forms.TextBox();
            this.addButton = new System.Windows.Forms.Button();
            this.fileListBox = new System.Windows.Forms.ListBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.sysLabel = new System.Windows.Forms.Label();
            this.branchLabel = new System.Windows.Forms.Label();
            this.donotAddButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // addLabel
            // 
            this.addLabel.AutoSize = true;
            this.addLabel.Location = new System.Drawing.Point(12, 93);
            this.addLabel.Name = "addLabel";
            this.addLabel.Size = new System.Drawing.Size(236, 20);
            this.addLabel.TabIndex = 0;
            this.addLabel.Text = "Enter what you would like to add";
            // 
            // addBox
            // 
            this.addBox.Location = new System.Drawing.Point(7, 125);
            this.addBox.Name = "addBox";
            this.addBox.Size = new System.Drawing.Size(323, 26);
            this.addBox.TabIndex = 1;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(34, 157);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 36);
            this.addButton.TabIndex = 2;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // fileListBox
            // 
            this.fileListBox.FormattingEnabled = true;
            this.fileListBox.ItemHeight = 20;
            this.fileListBox.Location = new System.Drawing.Point(7, 272);
            this.fileListBox.Name = "fileListBox";
            this.fileListBox.Size = new System.Drawing.Size(323, 84);
            this.fileListBox.TabIndex = 3;
            this.fileListBox.DoubleClick += new System.EventHandler(this.fileListBox_DoubleClick);
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(3, 221);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(277, 20);
            this.nameLabel.TabIndex = 4;
            this.nameLabel.Text = "Files changed or added, but unstaged";
            // 
            // sysLabel
            // 
            this.sysLabel.AutoSize = true;
            this.sysLabel.Location = new System.Drawing.Point(12, 18);
            this.sysLabel.Name = "sysLabel";
            this.sysLabel.Size = new System.Drawing.Size(62, 20);
            this.sysLabel.TabIndex = 5;
            this.sysLabel.Text = "System";
            // 
            // branchLabel
            // 
            this.branchLabel.AutoSize = true;
            this.branchLabel.Location = new System.Drawing.Point(12, 47);
            this.branchLabel.Name = "branchLabel";
            this.branchLabel.Size = new System.Drawing.Size(60, 20);
            this.branchLabel.TabIndex = 6;
            this.branchLabel.Text = "Branch";
            // 
            // donotAddButton
            // 
            this.donotAddButton.Location = new System.Drawing.Point(217, 157);
            this.donotAddButton.Name = "donotAddButton";
            this.donotAddButton.Size = new System.Drawing.Size(113, 36);
            this.donotAddButton.TabIndex = 7;
            this.donotAddButton.Text = "DO NOT Add";
            this.donotAddButton.UseVisualStyleBackColor = true;
            this.donotAddButton.Click += new System.EventHandler(this.donotAddButton_Click);
            // 
            // AddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 368);
            this.Controls.Add(this.donotAddButton);
            this.Controls.Add(this.branchLabel);
            this.Controls.Add(this.sysLabel);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.fileListBox);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.addBox);
            this.Controls.Add(this.addLabel);
            this.Name = "AddForm";
            this.Text = "AddForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label addLabel;
        private System.Windows.Forms.TextBox addBox;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.ListBox fileListBox;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label sysLabel;
        private System.Windows.Forms.Label branchLabel;
        private System.Windows.Forms.Button donotAddButton;
    }
}