﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GitMonster
{
    public partial class BranchSelector : Form
    {
        public List<string> branchList = new List<string>();
        public string thistype = "", thisdate="";
    

        public BranchSelector(List<string> importList, string thissys, string type, string date)
        {



            InitializeComponent();

            branchLabel.Text = "For System: " + thissys;

            thistype = type;
            thisdate = date;

            branchList = importList;
            BindingSource bs = new BindingSource();
            bs.DataSource = branchList;

            branchBox.DataSource = bs;

        }

        private void branchBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //BindingSource bs = new BindingSource();
            //bs.DataSource = branchList;
            //branchBox.DataSource = bs;
        }

        private void branchSelectorButton_Click(object sender, EventArgs e)
        {
            string selection = branchBox.Text;

            if(selection.Contains("Please Select the Correct Branch"))
            {
                selection = "nope";

            }

            Program.thisbranchselected(selection);

            DialogResult = DialogResult.OK;
            

        }

        private void newBranchButton_Click(object sender, EventArgs e)
        {
            newbranchform newBranch = new newbranchform(thistype, thisdate);
            DialogResult brnchrslt = newBranch.ShowDialog();

            DialogResult = DialogResult.OK;
        }

        private void skipButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
