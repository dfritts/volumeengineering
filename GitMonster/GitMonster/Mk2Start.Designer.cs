﻿namespace GitMonster
{
    partial class Mk2Start
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dueDatePicker = new System.Windows.Forms.DateTimePicker();
            this.typeBox = new System.Windows.Forms.ComboBox();
            this.dueDateLabel = new System.Windows.Forms.Label();
            this.runButton = new System.Windows.Forms.Button();
            this.folderBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // dueDatePicker
            // 
            this.dueDatePicker.Location = new System.Drawing.Point(65, 306);
            this.dueDatePicker.Name = "dueDatePicker";
            this.dueDatePicker.Size = new System.Drawing.Size(200, 26);
            this.dueDatePicker.TabIndex = 0;
            // 
            // typeBox
            // 
            this.typeBox.FormattingEnabled = true;
            this.typeBox.Items.AddRange(new object[] {
            "CCR",
            "REL"});
            this.typeBox.Location = new System.Drawing.Point(65, 358);
            this.typeBox.Name = "typeBox";
            this.typeBox.Size = new System.Drawing.Size(200, 28);
            this.typeBox.TabIndex = 1;
            this.typeBox.Text = "Select the Type";
            // 
            // dueDateLabel
            // 
            this.dueDateLabel.AutoSize = true;
            this.dueDateLabel.Location = new System.Drawing.Point(61, 283);
            this.dueDateLabel.Name = "dueDateLabel";
            this.dueDateLabel.Size = new System.Drawing.Size(154, 20);
            this.dueDateLabel.TabIndex = 2;
            this.dueDateLabel.Text = "Select the Due Date";
            // 
            // runButton
            // 
            this.runButton.Location = new System.Drawing.Point(111, 415);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(75, 31);
            this.runButton.TabIndex = 3;
            this.runButton.Text = "Run";
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.runButton_Click);
            // 
            // folderBox
            // 
            this.folderBox.Location = new System.Drawing.Point(65, 239);
            this.folderBox.Name = "folderBox";
            this.folderBox.Size = new System.Drawing.Size(200, 26);
            this.folderBox.TabIndex = 4;
            this.folderBox.Text = "Enter Folder Location";
            // 
            // Mk2Start
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 450);
            this.Controls.Add(this.folderBox);
            this.Controls.Add(this.runButton);
            this.Controls.Add(this.dueDateLabel);
            this.Controls.Add(this.typeBox);
            this.Controls.Add(this.dueDatePicker);
            this.Name = "Mk2Start";
            this.Text = "Mk2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dueDatePicker;
        private System.Windows.Forms.ComboBox typeBox;
        private System.Windows.Forms.Label dueDateLabel;
        private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.TextBox folderBox;
    }
}