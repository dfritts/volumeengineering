%Pulls the coordinate data out of a .json (or .geojson) file and outputs it
%into a .csv
%Notable functions: jsdondecode()
%Author: David Fritts

format long g 
fname='GanderDomestic_BVol.geojson';
fid=fopen(fname);
raw=fread(fid,inf);
str=char(raw');
fclose(fid);
val=jsondecode(str);
points=[val.features.geometry.coordinates(:,:,2) val.features.geometry.coordinates(:,:,1)];
output=squeeze(points)';
dlmwrite('coordinates.csv',output,'delimiter',',','precision',9);
winopen('coordinates.csv');