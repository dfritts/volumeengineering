%Takes in a set of coordinates and deletes linear combinations 
%Author: David Fritts

format long g 
rawcoor = csvread('montreal.csv');
lat = rawcoor(:,1);
long = rawcoor(:,2);

%latitude
latback = circshift(lat,-1);
latforward = circshift(lat,1);
latfilter = latback ==lat & latforward == lat;
% sum(latfilter)

%longitude
longback = circshift(long,-1);
longforward = circshift(long,1);
longfilter = longback ==long & longforward == long;
% sum(longfilter)

filter = ~(latfilter | longfilter);
% filter(1) = 1; %Adding back in the first coordinate
newcoor = rawcoor(filter,:);
% load coast;
% plot(long,lat);
% hold on
% plot(rawcoor(:,2),rawcoor(:,1),'-og');
% plot(newcoor(:,2),newcoor(:,1),'-xr');
% hold off

dlmwrite('shortenedmont.csv',newcoor,'Delimiter',',','precision',9);
% clear variables