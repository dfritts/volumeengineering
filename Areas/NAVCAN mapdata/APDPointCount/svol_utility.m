% SVOL_UTILITY ASIM service volume utility
%   OUT_SV = SVOL_UTILITY(VARS, SV_LIST, PARAM, VALUE) loads the ASIM 
%   service volumes based on the VARS path.  The function will work on the 
%   service volumes described by the SV_LIST which consists of the service 
%   volume FID numbers.  The following PARAM arguments are valid:
%
%       'merge' - if followed by true the utility will merge all service
%       volumes specified in the SV_LIST
%       'buffer_zone' - the service volumes listed will be enlarged by a
%       buffer zone of this size in nautical miles
%       'intrusion' - service volumes will have their regions reduced if
%       they cross into any other regions under this responsibility (RESP
%       field in the service_vol table).
%       'max_vertices' - reduces the number of points in the polygons to
%       this limit
%       'add' - add the service volume specified in the following
%       argument.  Required fields are:
%           X
%           Y
%           FIR
%           ICAOCODE
%           FIRname
%           SIC
%           SAC
%       any additional fields will be added, any others that are not
%       specified will be set to defaults.  If you add a field that does
%       not exist in the table the following will happen to all other
%       volumes:
%           Field contains a number:  All other volumes will be set to zero
%           Field is cell string: All other volumes will be set to empty
%              strings
%           
%   Additionally this utility will update the additional fields of the
%   service volume table

function out_sv = svol_utility(input1, sv_list, varargin)

if istable(input1)
    service_vol = input1;
elseif isstruct(input1)
    vars = input1;
    service_vol = [];
    load(vars.paths.service_vol);
else
    out_sv = [];
    return;
end

options = cell2struct(varargin(2:2:end), lower(varargin(1:2:end)), 2);

out_sv = [];
if ~isempty(sv_list)
    sv_mask = ismember(service_vol.FID_1, sv_list);
    out_sv = service_vol(sv_mask,:);
end

%% ADD
if isfield(options, 'add')
    out_sv = service_vol(1,:);
    new_sv = options.add;
    
    %set values that need to be set but aren't required by tool
    all_mcast = cell2mat(cellfun(@(x)sscanf(x,'%d.%d.%d.%d')', service_vol.mcast_ip,'UniformOutput',false));
    
    out_sv.FID_1 = max(service_vol.FID_1)+1;
    out_sv.Id = out_sv.FID_1;
    out_sv.mcast_ip = {['233.1.' num2str(max(all_mcast(:,3))+1) '.1']};
    out_sv.OOAT_Stage = 0;
    out_sv.CAT = 9;
    
    fieldz = new_sv.Properties.VariableNames;
    for ii=1:length(fieldz)
        out_sv.(fieldz{ii}) = new_sv.(fieldz{ii});
    end
    
    %check X/Y have NaN at end
    if ~isnan(out_sv.X{1}(end))
        out_sv.X{1}(end+1) = NaN;
        out_sv.Y{1}(end+1) = NaN;
    end
    
end

%% MERGE
if isfield(options, 'merge') %if set will merge all SVs in the sv_list
    all_polys_lat = {};
    all_polys_lon = {};
    for ii=1:size(out_sv,1)
        this_sv = out_sv(ii,:);
        
        [poly_lat, poly_lon, poly_break] = get_poly_llb(this_sv);
        
        curr_poly_index = 1;
        for mm=1:length(poly_break) %loop through sub polygons
            this_poly_lat = poly_lat(curr_poly_index:(poly_break(mm)-1));
            this_poly_lon = poly_lon(curr_poly_index:(poly_break(mm)-1));
            
            curr_poly_index = poly_break(mm)+1;
            
            if this_poly_lat(end)==this_poly_lat(1) && this_poly_lon(end)==this_poly_lon(1)
                this_poly_lat(end) = [];
                this_poly_lon(end) = [];
            end
            
            all_polys_lat = [all_polys_lat {this_poly_lat}]; %#ok<AGROW>
            all_polys_lon = [all_polys_lon {this_poly_lon}]; %#ok<AGROW>
        end
    end
    
    region_list = unique(out_sv.REGION);
    resp_list = unique(out_sv.RESP);
    supps_list = unique(out_sv.SUPPS);
    new_ceil = max(out_sv.Ceiling);
    new_floor = min(out_sv.Floor);
    
    out_sv = out_sv(1,:); % we are merging so just use the first
    sac = out_sv.SAC;
    sic_used = service_vol.SIC(service_vol.SAC==sac);
    
    [save_poly_lat, save_poly_lon] = merge_poly(all_polys_lat, all_polys_lon, 5*1852);
    
    for mm=1:length(save_poly_lat)
        save_poly_lat{mm}((end+1):(end+2)) = [save_poly_lat{mm}(1) NaN];
        save_poly_lon{mm}((end+1):(end+2)) = [save_poly_lon{mm}(1) NaN];
    end
    
    out_sv.X = {cell2mat(save_poly_lon)};
    out_sv.Y = {cell2mat(save_poly_lat)};
    
    %fix other parameters
    out_sv.FID_1 = out_sv.FID_1+1000;
    out_sv.Id = out_sv.FID_1;
    out_sv.FIR = {'NIL'};
    if length(region_list)>1
        out_sv.REGION = {'NIL'};
    else
        out_sv.REGION = region_list;
    end
    out_sv.KIND = {'FIR'};
    out_sv.ICAOCODE = {[out_sv.ICAOCODE 'M']};
    if length(resp_list)>1
        out_sv.RESP = {'NIL'};
    else
        out_sv.RESP = resp_list;
    end
    out_sv.FIRname = {[out_sv.FIRname ' Merge']};
    if length(supps_list)>1
        out_sv.SUPPS = {'SUPPS no'};
    else
        out_sv.SUPPS = supps_list;
    end
    out_sv.SIC = max(sic_used)+1;
    if out_sv.SIC==256
        out_sv.SAC=255;
        out_sv.SIC=255;
    end
    
    out_sv.mcast_ip = ['233.1.16' num2str(mod(160+out_sv.SAC,255)) '.' num2str(out_sv.SIC)];
    
    out_sv.ANSP = {' '};
    out_sv.Ceiling = new_ceil;
    out_sv.Floor = new_floor;
end

%% BUFFER ZONE
%user wants to add a buffer zone to the SVs (value is in NM)
if isfield(options, 'buffer_zone')
    bufzone = options.buffer_zone*1852;
    for ii=1:size(out_sv,1)
        this_sv = out_sv(ii,:);
        
        [poly_lat, poly_lon, poly_break] = get_poly_llb(this_sv);
        
        save_poly_lat = cell(1,length(poly_break));
        save_poly_lon = cell(1,length(poly_break));
        curr_poly_index = 1;
        for mm=1:length(poly_break) %loop through sub polygons
            this_poly_lat = poly_lat(curr_poly_index:(poly_break(mm)-1));
            this_poly_lon = poly_lon(curr_poly_index:(poly_break(mm)-1));
            curr_poly_index = poly_break(mm)+1;
            
            [big_poly_lat, big_poly_lon] = enlarge_geo_poly(this_poly_lat, this_poly_lon, bufzone);
            
            save_poly_lat(mm) = {big_poly_lat};
            save_poly_lon(mm) = {big_poly_lon};
        end
        
        dead_poly = cellfun('isempty',save_poly_lat);
        save_poly_lat(dead_poly) = [];
        save_poly_lon(dead_poly) = [];
        
        %check if any sub polygon points overlap and if so remove them and
        %merge the two polygons
        if length(save_poly_lat)>1
            [save_poly_lat, save_poly_lon] = merge_poly(save_poly_lat, save_poly_lon);
        end
        
        for mm=1:length(save_poly_lat)
            save_poly_lat{mm}((end+1):(end+2)) = [save_poly_lat{mm}(1) NaN];
            save_poly_lon{mm}((end+1):(end+2)) = [save_poly_lon{mm}(1) NaN];
        end
        
        out_sv.X = {cell2mat(save_poly_lon)};
        out_sv.Y = {cell2mat(save_poly_lat)};
        
        this_sv.X = {cell2mat(save_poly_lon)};
        this_sv.Y = {cell2mat(save_poly_lat)};
        
        out_sv(ii,:) = this_sv;
    end
end

%% PREVENT INTRUSION
if isfield(options, 'intrusion')
    intrusion_resp = options.intrusion; %compare to service_vol.RESP
    intrusion_sv = service_vol(strcmp(service_vol.RESP, intrusion_resp),:);
    
    for ii=1:size(out_sv,1)
        this_sv = out_sv(ii,:);
        
        for jj=1:size(intrusion_sv,1)
            [this_sv.X{1}, this_sv.Y{1}] = polybool('subtraction', this_sv.X{1}, this_sv.Y{1}, intrusion_sv.X{jj}, intrusion_sv.Y{jj});
        end
        
        out_sv(ii,:) = this_sv;
    end
end


%% REDUCE VERTICES
if isfield(options, 'max_vertices')
    max_vert = options.max_vertices;
    for ii=1:size(out_sv,1)
        this_sv = out_sv(ii,:);
        
        [poly_lat, poly_lon, poly_break] = get_poly_llb(this_sv);
        
        save_poly_lat = cell(1,length(poly_break));
        save_poly_lon = cell(1,length(poly_break));
        curr_poly_index = 1;
        for mm=1:length(poly_break) %loop through sub polygons
            this_poly_lat = poly_lat(curr_poly_index:(poly_break(mm)-1));
            this_poly_lon = poly_lon(curr_poly_index:(poly_break(mm)-1));
            curr_poly_index = poly_break(mm)+1;
            
            [reduced_poly_lat, reduced_poly_lon] = reduce_geo_poly_vertices(this_poly_lat, this_poly_lon, max_vert);
            reduced_poly_lat((end+1):(end+2)) = [reduced_poly_lat(1) NaN];
            reduced_poly_lon((end+1):(end+2)) = [reduced_poly_lon(1) NaN];
            
            save_poly_lat(mm) = {reduced_poly_lat};
            save_poly_lon(mm) = {reduced_poly_lon};
        end
        this_sv.X = {cell2mat(save_poly_lon)};
        this_sv.Y = {cell2mat(save_poly_lat)};
        
        out_sv(ii,:) = this_sv;
    end
end

%% CLEAN UP OUTPUT SVS
for ii=1:size(out_sv,1)
    
    lon = out_sv.X{ii}(1:end-1); %all have NaN at end
    lat = out_sv.Y{ii}(1:end-1);
    
    sys_center = [mean(lat) mean(lon)];
    [poly_x, poly_y] = geodetic2enu(lat, lon, zeros(size(lat)), sys_center(1), sys_center(2), 0, wgs84Ellipsoid);
    poly_xy = [poly_x' poly_y'];
    
    %set bounding box, area, perimeter, etc.
    out_sv.BoundingBox{ii} = [min(lon) min(lat); max(lon) max(lat)];
    out_sv.AREA(ii) = polyarea(poly_x, poly_y)/1000; %in KM
    out_sv.PERIMETER(ii) = sum(sqrt(sum((poly_xy-circshift(poly_xy,-1)).^2,2)))/1000; %in KM
    out_sv.LONGcent(ii) = sys_center(2);
    out_sv.LATcent(ii) = sys_center(1);
    
    if isfield(options, 'add') %out_sv needs to be updated to have the original list included
        
        new_fields = out_sv.Properties.VariableNames;
        old_fields = service_vol.Properties.VariableNames;
        
        for jj=1:length(new_fields)
            if ~any(strcmp(old_fields, new_fields{jj}))
                %check what data is in new field
                if iscell(out_sv.(new_fields{jj}))
                    old_values = cell(size(service_vol,1),1);
                    old_values(:) = {''};
                elseif isnumeric(out_sv.(new_fields{jj}))
                    old_values = zeros(size(service_vol,1),1);
                end
                
                service_vol.(new_fields{jj}) = old_values;
            end
        end
        
        service_vol(end+1,:) = out_sv; %#ok<AGROW>
        out_sv = service_vol;
    end
end


end

%get polygon lat/lon and breaks
function [lat, lon, brks] = get_poly_llb(sv)
lon = sv.X{1};
lat = sv.Y{1};

brks = find(isnan(lon));
if isempty(brks) %only one polygon with no NaN at the end
    lon(end+1) = NaN;
    lat(end+1) = NaN;
    brks = length(lon);
end
end

%merge polygon function
function [poly_lat, poly_lon] = merge_poly(poly_lat, poly_lon, near_thresh)
near = true;
if nargin==2
    near = false;
    near_thresh = 0;
end

%order polygons in distance from first
poly_lat_center = cellfun(@(x)mean(x), poly_lat);
poly_lon_center = cellfun(@(x)mean(x), poly_lon);

dist = sqrt((poly_lon_center(1) - poly_lon_center).^2 + (poly_lat_center(1) - poly_lat_center).^2);
[~,I] = sort(dist);
poly_lat = poly_lat(I);
poly_lon = poly_lon(I);

poly_lat_big = cell(length(poly_lat),1);
poly_lon_big = cell(length(poly_lat),1);
if near %enlarge all the polygons
    for ii=1:length(poly_lat)
        [poly_lat_big{ii}, poly_lon_big{ii}] = enlarge_geo_poly(poly_lat{ii}, poly_lon{ii}, near_thresh);
    end
end

found_merge = true;
while found_merge
    found_merge = false;
    for mm=1:length(poly_lat)
        if found_merge
            break;
        end
        
        poly1_lat = poly_lat{mm};
        poly1_lon = poly_lon{mm};
        for nn=1:length(poly_lat)
            if found_merge
                break;
            end
            
            if mm==nn
                continue;
            end
            poly2_lat = poly_lat{nn};
            poly2_lon = poly_lon{nn};
            
            if near
                %use big polygons to check with inpolygon
                poly1_lat_big = poly_lat_big{mm};
                poly1_lon_big = poly_lon_big{mm};
                
                poly2_lat_big = poly_lat_big{nn};
                poly2_lon_big = poly_lon_big{nn};
                
                if isempty(poly1_lat_big)
                    poly1_lat_big = poly1_lat;
                    poly1_lon_big = poly1_lon;
                end
                if isempty(poly2_lat_big)
                    poly2_lat_big = poly2_lat;
                    poly2_lon_big = poly2_lon;
                end
                
                in1 = inpolygon(poly1_lon, poly1_lat, poly2_lon_big, poly2_lat_big);
                in2 = inpolygon(poly2_lon, poly2_lat, poly1_lon_big, poly1_lat_big);
            else
                in1 = inpolygon(poly1_lon, poly1_lat, poly2_lon, poly2_lat);
                in2 = inpolygon(poly2_lon, poly2_lat, poly1_lon, poly1_lat);
            end
            
            if any(in1) || any(in2)
                if all(in1) || ~any(in2)
                    poly_lon(mm) = [];
                    poly_lat(mm) = [];
                    if near
                        poly_lat_big(mm) = [];
                        poly_lon_big(mm) = [];
                    end
                    found_merge = true;
                elseif all(in2) || ~any(in1)
                    poly_lon(nn) = [];
                    poly_lat(nn) = [];
                    if near
                        poly_lat_big(nn) = [];
                        poly_lon_big(nn) = [];
                    end
                    found_merge = true;
                else
                    if in1(1)
                        %find first zero and make it first entry
                        in1_first = find(~in1,1);
                        
                        in1 = circshift(in1, -in1_first);
                        poly1_lat = circshift(poly1_lat, -in1_first);
                        poly1_lon = circshift(poly1_lon, -in1_first);
                    end
                    if in2(1)
                        %find first zero and make it first entry
                        in2_first = find(~in2,1);
                        
                        in2 = circshift(in2, -in2_first);
                        poly2_lat = circshift(poly2_lat, -in2_first);
                        poly2_lon = circshift(poly2_lon, -in2_first);
                    end
                    
                    in1_first = find(in1,1);
                    in1_last = find(in1,1,'last');
                    in2_first = find(in2,1);
                    
                    in1(in1_first) = false;
                    in1(in1_last) = false;
                    
                    poly1_lat(in1) = [];
                    poly1_lon(in1) = [];
                    poly2_lat(in2) = [];
                    poly2_lon(in2) = [];
                    
                    poly1_lat = circshift(poly1_lat,-in1_first);
                    poly1_lon = circshift(poly1_lon,-in1_first);
                    poly2_lat = circshift(poly2_lat,-in2_first+1);
                    poly2_lon = circshift(poly2_lon,-in2_first+1);
                    
                    merge_poly_lat = [poly1_lat poly2_lat];
                    merge_poly_lon = [poly1_lon poly2_lon];
                    
                    poly_lat{mm} = merge_poly_lat;
                    poly_lon{mm} = merge_poly_lon;
                    
                    if near %enlarge all the polygons
                        near_thresh = 5*1852;
                        [poly_lat_big{mm}, poly_lon_big{mm}] = enlarge_geo_poly(poly_lat{mm}, poly_lon{mm}, near_thresh);
                        
                        poly_lat_big(nn) = [];
                        poly_lon_big(nn) = [];
                    end
                    
                    poly_lon(nn) = [];
                    poly_lat(nn) = [];
                    found_merge = true;
                end
            end
        end
    end
end
end