---------------------------------------------------------------------------------------------------------------------------------------------------
Aireon APD Key Delivery
2016-03-28

Release new keys associated with BIOS v3.0000 timestamp 10030A10 and 10031C0C. Includes old keys as well.
---------------------------------------------------------------------------------------------------------------------------------------------------

Key File Assignments:
AHP S/W   CCS BIOS CI 8027900   BIOS Version       BIOS               ADS-B Key Files   
Version   Version               Timestamp (ESR)    Type
-------   -------------------   ---------------    ----------------   ---------------------------------------------------------------------------------------------
1.0401    1.1                   0E080D09           BIOS0              AESadsbsm_201402180640_0E080D09.key
1.0500    1.2                   0E081410           BIOS0              AESadsbsm_201402180640_0E081410.key
2.0000    2.0                   0F08180E           BIOS0              AESadsbsm_201402180640_0F08180E.key
2.1000    2.1                   0F0A170C           BIOS0              AESadsbsm_201402180640_0F0A170C.key
2.3000    2.2                   0F0B1312           BIOS0              AESadsbsm_201402180640_0F0B1312.key
3.0000    3.0000.201402180640   10030A10           BIOS1              AESadsbsm_201402180640_10030A10.key
3.0000    3.0000.201602090638   10031C0C           BIOS1              AESadsbsm_201602090638_10031C0C.key

Files Delivered:
File                                                          Description
---------------------------------------------------------     -------------------------------------------------------------------
./AESadsbsm_201402180640_0E080D09.key                         CCS BIOS CI 8027900 version 1.5 HPC Key File for BIOS0
./AESadsbsm_201402180640_0E081410.key                         CCS BIOS CI 8027900 version 1.6 HPC Key File for BIOS0
./AESadsbsm_201402180640_0F08180E.key                         CCS BIOS CI 8027900 version 2.0 HPC Key File for BIOS0
./AESadsbsm_201402180640_0F0A170C.key                         CCS BIOS CI 8027900 version 2.1 HPC Key File for BIOS0
./AESadsbsm_201402180640_0F0B1312.key                         CCS BIOS CI 8027900 version 2.2 HPC Key File for BIOS0
./AESadsbsm_201402180640_10030A10.key                         CCS BIOS CI 8027900 version 3.0000.201402180640 HPC Key File for BIOS1
./AESadsbsm_201602090638_10031C0C.key                         CCS BIOS CI 8027900 version 3.0000.201602090638 HPC Key File for BIOS1
./README.txt                                                  The file you are reading

Files Deprecated:
File                                                          Comments
---------------------------------------------------------     -------------------------------------------------------------------
./AESadsbsm_201602090638_10020909.key                         This key file was built in advance of BIOS v3.0000 under the
                                                              assumption that there would be no BIOS changes before building BIOS
                                                              v3.0000. Since then, sveral PTRs have been incorporated into BIOS
                                                              v3.0000, and therefore its timestamp version ID has changed from 
                                                              0x10020909. This key file is no longer necessary as it is not 
                                                              associated with any existing BIOS.